from django.db import models
from django.conf import settings
from employees.models import Department, Employee, Profile
from Salary.fun_SingleMultiUser import SingleMultiUser_fun


class TimeSheet(models.Model):
    Working_Hour_CHOISES =(    #to make salary_base choice without foreignkey in models
        ('8','Full_day'),
        ('4','Half_day'),
        ('0','absent')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    Emp_name =  models.ForeignKey(Employee,null=True, on_delete=models.CASCADE )
    Pro_name =  models.ForeignKey(Profile,null=True, on_delete=models.CASCADE )
    Department = models.ForeignKey(Department,null=True, on_delete= models.CASCADE )
    Date = models.DateField(auto_now=False)
    working_hour = models.IntegerField(null=True, blank=True ,choices=Working_Hour_CHOISES)
    submit_date=models.DateTimeField(auto_now=False, null=True, blank=True )
    approve_date=models.DateTimeField(auto_now=False, null=True, blank=True)
    #overtime_hour = models.DecimalField(decimal_places= 0, max_digits= 2,blank=True, null=True)
    #alowance_category= models.DecimalField(decimal_places= 0, max_digits=2, blank=True, null=True)
    #bonus = models.DecimalField(decimal_places= 0, max_digits=2, blank=True, null=True)
    updated = models.DateTimeField(auto_now=True)
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['Emp_name','Pro_name','Date'],name='unique_timesheet')
            #each employee have only one timesheet each day 
        ]
    if SingleMultiUser_fun() == Employee:
        def __str__(self):
            return self.Emp_name.Full_name
    
    elif SingleMultiUser_fun() == Profile:
        def __str__(self):
            return self.Pro_name.Full_name.username



