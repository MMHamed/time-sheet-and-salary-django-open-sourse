import django_tables2 as tables
from django import forms
import itertools
from django_tables2.utils import A
from django.utils.safestring import mark_safe
from .models import TimeSheet
from employees.models import Employee, Department, Profile
from vacations.models import vacation
from Salary.fun_SingleMultiUser import SingleMultiUser_fun 

#class ChoiceColumn(tables.Column):  #that solution to add an input column has failed to implement
#    def __init__(self, choices, *args, **kwargs):
#       self.choices = choices
#        super(ChoiceColumn, self).__init__(*args, **kwargs)

#    def render(self,value):
#        ff = forms.ChoiceField(choices=self.choices)
#        return ff.widget.render(self.verbose_name, self.label_to_value(value))

#    def label_to_value(self , label):
#        for(v ,l) in self.choices:
#            if l == label:
 #               return v

####to add an input column to the tables #####

TEMPLATE ="""    
<select maxlenght='10' name='Working_Hour' class="btn btn-outline-primary Working_Hour" data-placeholder='choose one' disabled= 'disabled'>
    <option value=''></option>
    <option value='0' id='0'>absent</option>
    <option value='4' id='4'>Half_day</option>
    <option value='8' id='8'>Full_day</option>
</select>

"""

class TStable(tables.Table):
    Working_Hour = tables.TemplateColumn(TEMPLATE, orderable=False) #work to add an input column to the table!!
    row_number = tables.Column(empty_values=())   #to add a row that increament each row->it work
    class Meta:
        model= SingleMultiUser_fun()
        fields= ['id', 'Full_name', 'Department' ]
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'id', 'Full_name', 'Department', 'Working_Hour']
        orderable = False  #to disable orderable in the hole table
    
    def __init__(self, *args, **kwargs):
        super(TStable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return 'Row %d' % next(self.counter)

    # def render_Working_Hour(self,value, record):
    #     qs = vaction.objects.get()
    #     return mark_safe('''<div>approved vacation</div>''')

        
    #def render_id(self, value): #this work. it is use to add <> to the ID column. it is used to add any constant to a column
    #    return '<%s>' % value

    #working_hour = ChoiceColumn(TimeSheet.working_hour, verbose_name='working_hour')

class TSListTBL(tables.Table):
    row_number = tables.Column(empty_values=()) #to add a row that increament each row->it work
    Employee_name =tables.Column(empty_values=(), orderable=False)
    Employee_ID = tables.Column(empty_values=(), orderable=False) #to make a row on click go to timeSheet; not work as the below overwrite
    #Emp_name = tables.Column(verbose_name='Employee_ID') # not working alone, we have to combine it with the above
    Department= tables.Column()
    class Meta:
        model = TimeSheet
        fields= [ 'Department', 'dcount']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'Employee_ID', 'Employee_name', 'Department', 'dcount']

     
   
    
    def __init__(self, *args, **kwargs):
        super(TSListTBL, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return 'Row %d' % next(self.counter)

    #def render_Emp_name(self, value): #this work. it replace the id in that column with the Full_name
    #    qs= Employee.objects.get(id = value)
    #    return '%s' % qs.Full_name
    
    def render_Department(self, value): #this work. it replace the id in that column with the departement name
        qs= Department.objects.get(id = value)
        return '%s' % qs

    def render_Employee_ID(self,value,record):
        if SingleMultiUser_fun() == Employee:
            return mark_safe('''<a href=/%s/approveTS>%s<a>''' % (record['Emp_name'],record['Emp_name']))
        elif SingleMultiUser_fun() == Profile:
            return mark_safe('''<a href=/%s/approveTS>%s<a>''' % (record['Pro_name'],record['Pro_name']))


    def render_Employee_name(self, value ,record): #this work. with its declaration above we insert a new column that contain the name of the employee!!! 
        if SingleMultiUser_fun() == Employee:
            qs= SingleMultiUser_fun().objects.get(id = record['Emp_name'])
            return mark_safe('''<a href=/%s/approveTS>%s<a>''' % (record['Emp_name'],qs))
        elif SingleMultiUser_fun() == Profile:
            qs= SingleMultiUser_fun().objects.get(id = record['Pro_name'])
            return mark_safe('''<a href=/%s/approveTS>%s<a>''' % (record['Pro_name'],qs))

class ApTsTBL(tables.Table):
    row_number = tables.Column(empty_values=())
    class Meta:
        model = TimeSheet
        fields= ['Date', 'working_hour','submit_date']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'Date', 'working_hour','submit_date']

    def __init__(self, *args, **kwargs):
        super(ApTsTBL, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return 'Row %d' % next(self.counter)

class search_timesheet(tables.Table):
    row_number = tables.Column(empty_values=())
    Request_status = tables.Column(empty_values=())
    class Meta:
        model = TimeSheet
        fields= ['Emp_name','Date', 'working_hour']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number','Emp_name', 'Date', 'working_hour','Request_status']

    def __init__(self, *args, **kwargs):
        super(search_timesheet, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return 'Row %d' % next(self.counter)

    def render_Request_status(self, value, record):
        #qs= TimeSheet.objects.get(Emp_name = record.Emp_name, Date= record.Date)
        if record.submit_date:
            if record.approve_date:
                stat = 'approved'
            else:
                stat = 'submitted'
        else:
            stat= 'rejected'

        return '%s' % stat





        


 