from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django_tables2 import RequestConfig
from django.utils   import timezone
import time
from datetime import datetime, date , timedelta
from django.contrib.auth.decorators import login_required 
from django.core import serializers
from django.db.models import Count
from django.db.models import Q

from .models import TimeSheet
from .tables import TStable, TSListTBL, ApTsTBL, search_timesheet
from employees.models import Employee , Profile
from django.contrib.auth.models import User
from searches.forms import SearchFullName
from vacations.models import vacation
from settings.models import NationalVacation, WorkingDay
from Salary.fun_SingleMultiUser import SingleMultiUser_fun 

@login_required
def GrpTimesheet_view(request):
    #*********function to get the vacation and send it to js *********
    def vacList(TsDate):
        if SingleMultiUser_fun() == Employee:
            qs= TimeSheet.objects.filter(Date = TsDate).order_by('Emp_name') #to get the timesheet saved in that day
        elif SingleMultiUser_fun() == Profile:
            qs= TimeSheet.objects.filter(Date = TsDate).order_by('Pro_name')
        qss= vacation.objects.filter(vacation_date = TsDate, approve_date__isnull= False, submit_date__isnull = False)
        apVacEmp = []  #define an array for emp who have vaction that day
        apExVacEmp =[] #define an array for emp who exceed the limit of vacation
        for x in qss:  
            apVacEmp.append(x.Emp_name_id) #put the employee in vacation that day in an array  
        for x in apVacEmp: #to check if any one of them exceed the vacation limit
            qss1 = SingleMultiUser_fun().objects.get (id = x)
            emp_date = qss1.Emp_date  # employement date
            emp_date_month = int(emp_date.month or 0)
            TsDate_month = int(TsDate.month or 0)
            TsDate_year = int(TsDate.year or 0)
            # to check if the employement data start month is in this year or in last year
            if  emp_date_month < TsDate_month:
                VacStartDate = date(year= TsDate_year,month= emp_date_month ,day= 1)
                VacEndDate = date(year= (TsDate_year+1),month= emp_date_month ,day= 1)
            else:
                VacStartDate = date(year= (TsDate_year-1),month= emp_date_month,day= 1)
                VacEndDate = date(year= TsDate_year,month= emp_date_month,day= 1)

            vacYear = qss1.Vacation_per_year # number of vacation day per year in database
                
            qss2 = vacation.objects.filter(Emp_name_id = x, vacation_date__range = (VacStartDate, VacEndDate) ).exclude(approve_date = None).aggregate(AVnum = Count('vacation_date'))   
            NumAppVac = int(qss2['AVnum'] or 0) #number of approved vacation days
            
            if NumAppVac > vacYear:
                apExVacEmp.append(x) #if the employee exceed the vacation limit put him in that array

        Weekend = WorkingDay.objects.filter(workDay = False)
        we=[]
        if Weekend:
            for x in Weekend:
                we.append(x.days)
        if TsDate.strftime("%A") in we:
            WE = 'It is a weekend'
            
        else:
            WE = None

        Nvac =  NationalVacation.objects.filter(Date = TsDate)
        NV=[]
        if Nvac:
            NH = 'It is a National vacation'
            
        else:
            NH = None      
        
        # apVacEmp = serializers.serialize('json',apVacEmp)  
        # apExVacEmp = serializers.serialize('json',apExVacEmp)
        apVaclist = {'apVacEmp':apVacEmp , 'apExVacEmp':apExVacEmp, 'weekend':WE , 'nationalVaction': NH }
        
        return apVaclist
    #*********end of function ***************
    nows = timezone.now()
    if SingleMultiUser_fun()==Profile:
        table = TStable(SingleMultiUser_fun().objects.filter(TimeSheet_base= 'daily').exclude(Full_name__username__icontains = 'admin', ).order_by('pk'))
    elif SingleMultiUser_fun()== Employee:
        table = TStable(SingleMultiUser_fun().objects.filter(TimeSheet_base= 'daily').exclude(Full_name__icontains = 'admin').order_by('pk'))
        
    #table = TStable(Employee.objects.all(),order_by="id") #use if the table is orderable=true
    RequestConfig(request).configure(table)
    sort_by ='id'
    context = {'table':table ,"nows":nows, 'sort_by':sort_by}
    template = 'timesheets/grpTS.html'
    if request.GET:  #this will be received from table2 sort
        sort_by = request.GET.get('sort',None)
        context = {'table':table ,"nows":nows, 'sort_by':sort_by}
    if request.POST:
        TsDate = request.POST.get('TsDate',None)
        TsDate = datetime.strptime(TsDate, '%Y-%m-%d') #to convert it from string to date
        # print('*******************')
        # print(TsDate, type(TsDate))
        if SingleMultiUser_fun()==Profile: 
            qs= TimeSheet.objects.filter(Date = TsDate).exclude(Pro_name = None)
        elif SingleMultiUser_fun()== Employee:
            qs= TimeSheet.objects.filter(Date = TsDate).exclude(Emp_name = None)

        if qs.exists():
            if SingleMultiUser_fun()==Profile:
                if TimeSheet.objects.filter(Date = TsDate).exclude((Q(submit_date = None ) & Q(approve_date = None )) | Q(Pro_name = None)).exists():
                    message_return = 'Submit/Approve'
                else:
                    message_return = 'Saved'
            elif SingleMultiUser_fun()== Employee:
                if TimeSheet.objects.filter(Date = TsDate).exclude((Q(submit_date = None ) & Q(approve_date = None )) | Q(Emp_name = None)).exists():
                    message_return = 'Submit/Approve'
                else:
                    message_return = 'Saved'
            
            # #*********to get the vacation and send it to js *********
            # qs= TimeSheet.objects.filter(Date = TsDate).order_by('Emp_name') #to get the timesheet saved in that day
            # qss= vacation.objects.filter(vacation_date = TsDate, approve_date__isnull= False, submit_date__isnull = False)
            # apVacEmp = []  #define an array for emp who have vaction that day
            # apExVacEmp =[] #define an array for emp who exceed the limit of vacation
            # for x in qss:  
            #     apVacEmp.append(x.Emp_name_id) #put the employee in vacation that day in an array  
            # for x in apVacEmp: #to check if any one of them exceed the vacation limit
            #     qss1 = Employee.objects.get (id = x)
            #     emp_date = qss1.Emp_date  # employement date
            #     emp_date_month = int(emp_date.month or 0)
            #     # to check if the employement data start month is in this year or in last year
            #     if  emp_date_month < int(TsDate.month or 0):
            #         VacStartDate = date(year= int(TsDate.year or 0),month= emp_date_month ,day= 1)
            #         VacEndDate = date(year= (int(TsDate.year or 0)+1),month= emp_date_month ,day= 1)
            #     else:
            #         VacStartDate = date(year= (int(TsDate.year or 0)-1),month= emp_date_month,day= 1)
            #         VacEndDate = date(year= int(TsDate.year or 0),month= emp_date_month,day= 1)

            #     vacYear = qss1.Vacation_per_year # number of vacation day per year in database
                    
            #     qss2 = vacation.objects.filter(Emp_name_id = x, vacation_date__range = (VacStartDate, VacEndDate) ).exclude(approve_date = None).aggregate(AVnum = Count('vacation_date'))   
            #     NumAppVac = int(qss2['AVnum'] or 0) #number of approved vacation days
                
            #     if NumAppVac > vacYear:
            #         apExVacEmp.append(x) #if the employee exceed the vacation limit put him in that array
                   
            
            # apVacEmp = serializers.serialize('json',apVacEmp)  
            # apExVacEmp = serializers.serialize('json',apExVacEmp)
            apVacEmp = vacList(TsDate)['apVacEmp']
            apExVacEmp = vacList(TsDate)['apExVacEmp']
            weekend = vacList(TsDate)['weekend']
            NaVac = vacList(TsDate)['nationalVaction']
            qs = serializers.serialize('json',qs)
            context = {'message':message_return ,'weekend':weekend, 'NaVac':NaVac, 'qs':qs, 'apVacEmp':apVacEmp, 'apExVacEmp':apExVacEmp }
            return JsonResponse(context)
        else:
            apVacEmp = vacList(TsDate)['apVacEmp']
            apExVacEmp = vacList(TsDate)['apExVacEmp']
            weekend = vacList(TsDate)['weekend']
            NaVac = vacList(TsDate)['nationalVaction']
            message_return = "not Ceated Yet"
            context = {'message1':message_return, 'weekend':weekend, 'NaVac':NaVac, 'apVacEmp':apVacEmp, 'apExVacEmp':apExVacEmp }
            return JsonResponse(context)
    
    return render(request, template , context)

@login_required
def timesheet_save(request):
    if request.POST:  #to save in database
        if request.POST.get('status') == 'save': #save in database
            tsDate = request.POST.get('TsDate',None)
            RowNum = request.POST.get('RowNum',None)
            RowNum = int(RowNum) #should be convert to integer to be use in for loop
            for i in range(RowNum):
                data = request.POST.getlist(f'res[{i}][]') #getlist if the data in the form of array
                #pls don't forget the f''
                #print (data[0] +' , ' +data[1])
                emp = SingleMultiUser_fun().objects.get(id = data[0] )
                #print(emp.Department)
                if data[1] == '':
                    data[1] = None
                if SingleMultiUser_fun() == Employee:
                    obj , created = TimeSheet.objects.update_or_create(
                        Emp_name_id = data[0],
                        Pro_name_id = None,
                        Department = emp.Department,
                        Date = tsDate,
                        defaults={'working_hour': data[1],'user':request.user } #the field which will be update if the constrain-unique found 
                )
                elif SingleMultiUser_fun() == Profile:
                    obj , created = TimeSheet.objects.update_or_create(
                        Emp_name_id = None,
                        Pro_name_id = data[0],
                        Department = emp.Department,
                        Date = tsDate,
                        defaults={'working_hour': data[1],'user':request.user } #the field which will be update if the constrain-unique found 
                )
                #print(created)
                if created: 
                    message_return = 'Saved successfully'
                else:
                    message_return = 'Updated successfully'
    
        elif request.POST.get('status') == 'Submit_approve':   #to submit and approve 
            SubApdate = timezone.now()
            TsDate = request.POST.get('TsDate',None)
            RowNum = request.POST.get('RowNum',None)
            RowNum = int(RowNum) #should be convert to integer to be use in for loop
            for i in range(RowNum):
                data = request.POST.getlist(f'res[{i}][]') #getlist if the data in the form of array
                #pls don't forget the f''
                if data[1] == '':
                    data[1] = None
                emp = SingleMultiUser_fun().objects.get(id = data[0] )
                if SingleMultiUser_fun() == Employee:
                    obj , created = TimeSheet.objects.update_or_create(
                            Emp_name_id = data[0],
                            Pro_name_id = None,
                            Department = emp.Department,
                            Date = TsDate,
                            defaults={'working_hour': data[1],'submit_date': SubApdate,'approve_date': SubApdate ,'user':request.user } #the field which will be update if the constrain-unique found 
                        )
                elif SingleMultiUser_fun() == Profile:
                    obj , created = TimeSheet.objects.update_or_create(
                            Emp_name_id = None,
                            Pro_name_id = data[0],
                            Department = emp.Department,
                            Date = TsDate,
                            defaults={'working_hour': data[1],'submit_date': SubApdate,'approve_date': SubApdate ,'user':request.user } #the field which will be update if the constrain-unique found 
                        )
                if created: 
                    message_return = 'Saved/Submit/Approve successfully'
                else:
                    message_return = 'Submit/Approve successfully'
        
        elif request.POST.get('status') == 'saveSgTS' or request.POST.get('status') == 'submitSgTS':
            SubApdate = timezone.now()
            emp_ID = request.POST.get('emp_id',None)
            TSMonth = request.POST.get('TSMonth',None)
            TSYear = request.POST.get('TSYear',None)
            RowNum = request.POST.get('RowNum',None)
            RowNum = int(RowNum or 0) #should be convert to integer to be use in for loop
            TSMonth = int(TSMonth or 0)
            TSYear = int(TSYear or 0)
            emp_ID = int(emp_ID or 0)
            #print('***********')
            #print(emp_ID, TSMonth, TSYear )
            emp = SingleMultiUser_fun().objects.get(id = emp_ID )
            if request.POST.get('status') == 'saveSgTS':
                submit_date = None
            if request.POST.get('status') == 'submitSgTS':
                submit_date = SubApdate

            for i in range(RowNum):
                data = request.POST.getlist(f'res[{i}][]') #getlist if the data in the form of array
                #pls don't forget the f''
                if data[1] == '':
                    data[1] = None
                if SingleMultiUser_fun() == Employee:
                    obj , created = TimeSheet.objects.update_or_create(
                            Emp_name_id = emp_ID,
                            Pro_name_id = None,
                            Department = emp.Department,
                            Date = date(year=TSYear, month=TSMonth ,day=int(data[0])),
                            defaults={'working_hour': data[1],'submit_date': submit_date,'user':request.user } #the field which will be update if the constrain-unique found 
                        )
                elif SingleMultiUser_fun() == Profile:
                    obj , created = TimeSheet.objects.update_or_create(
                            Emp_name_id = None,
                            Pro_name_id = emp_ID,
                            Department = emp.Department,
                            Date = date(year=TSYear, month=TSMonth ,day=int(data[0])),
                            defaults={'working_hour': data[1],'submit_date': submit_date,'user':request.user } #the field which will be update if the constrain-unique found 
                        )
                if created: 
                    message_return = 'Created successfully'
                else:
                    message_return = 'Saved/Submit successfully'


                print(message_return)
    return HttpResponse(message_return)
    #return render(request,'timesheets/singleTs.html',{'message_return':message_return} )

@login_required
def Sgltimesheet_view(request):
    template = 'timesheets/singleTs.html'  #'timesheets/grpTS.html'
    form = SearchFullName()
    context = {"form": form}
    if request.POST:
        form = SearchFullName(request.POST, None)
        Emp_ID = request.POST.get('employee_list',None)
        TSMonth = request.POST.get('TSdate_month',None)
        TSYear = request.POST.get('TSdate_year',None)
        Emp_ID = int(Emp_ID or 0)
        TSYear = int(TSYear or 0)
        TSMonth = int(TSMonth or 0)
        TSDates= date(year= TSYear,month= TSMonth,day= 1)    
        #print(type(Emp_ID),  TSDates )
        if SingleMultiUser_fun() == Profile:
            user = request.user   #User.objects.get(pk = Emp_ID)
            if user.groups.filter(name="Self Timesheet and Vacation Request").exists():
                Emp_ID = Profile.objects.get(Full_name = Emp_ID ).id
        # print(Emp_ID, TSDates )
        try: 
            Emp_name =  SingleMultiUser_fun().objects.get(pk =Emp_ID, TimeSheet_base ='monthly')
            
        except: 
            Emp_name = None
            data= {'error_message': 'please select a valid Emloyee'}
            context['data'] = data
            TS_status = 'Unknown'
            qs = 'None'
        else:
            error_message=''
            #for timesheet status 
            if SingleMultiUser_fun() == Employee: 
                qs = TimeSheet.objects.filter(Date__year=TSYear, Date__month=TSMonth, Emp_name_id=Emp_ID ).order_by('Date')
                if qs.exists():
                    if TimeSheet.objects.filter(Date__year=TSYear, Date__month=TSMonth, Emp_name_id=Emp_ID, submit_date__isnull=False, approve_date__isnull=True ).exists():
                        TS_status = 'submited'
                    elif TimeSheet.objects.filter(Date__year=TSYear, Date__month=TSMonth, Emp_name_id=Emp_ID, submit_date__isnull=False, approve_date__isnull=False ).exists():    
                        TS_status = 'approved'
                    else:
                        TS_status = 'created'
                else:
                    TS_status ='not created yet'
                    qs ='None'
            elif SingleMultiUser_fun() == Profile:
                qs = TimeSheet.objects.filter(Date__year=TSYear, Date__month=TSMonth, Pro_name_id=Emp_ID ).order_by('Date')
                if qs.exists():
                    if TimeSheet.objects.filter(Date__year=TSYear, Date__month=TSMonth, Pro_name_id=Emp_ID, submit_date__isnull=False, approve_date__isnull=True ).exists():
                        TS_status = 'submited'
                    elif TimeSheet.objects.filter(Date__year=TSYear, Date__month=TSMonth, Pro_name_id=Emp_ID, submit_date__isnull=False, approve_date__isnull=False ).exists():    
                        TS_status = 'approved'
                    else:
                        TS_status = 'created'            
                else:
                    TS_status ='not created yet'
                    qs ='None'
            # print('*************')
            # print(qs, Emp_ID, TS_status)
            #get the vacation apporved query of this month
            if SingleMultiUser_fun() == Employee:
                qss = vacation.objects.filter(Emp_name = Emp_ID , vacation_date__month= TSMonth, vacation_date__year = TSYear, approve_date__isnull= False, submit_date__isnull = False )
            elif SingleMultiUser_fun() == Profile:
                qss = vacation.objects.filter(Pro_name = Emp_ID , vacation_date__month= TSMonth, vacation_date__year = TSYear, approve_date__isnull= False, submit_date__isnull = False )
            apVac = []  #define an array
            for x in qss:  #make an array with the vacation date on this month
                apVac.append(x.vacation_date)
            numVac = len(apVac)
            # print('***************')
            # print (numVac, apVac )
                    
            qss1 = SingleMultiUser_fun().objects.get (pk = Emp_ID)
            emp_date = qss1.Emp_date  # employement date
            emp_date_month = int(emp_date.month or 0)
            # to check if the employement data start month is in this year or in last year
            if  emp_date_month < TSMonth:
                VacStartDate = date(year= TSYear,month= emp_date_month ,day= 1)
                VacEndDate = date(year= (TSYear+1),month= emp_date_month ,day= 1)
            else:
                VacStartDate = date(year= (TSYear-1),month= emp_date_month,day= 1)
                VacEndDate = date(year= TSYear,month= emp_date_month,day= 1)

            vacYear = qss1.Vacation_per_year # number of vacation day per year
            if SingleMultiUser_fun() == Employee:       
                qss2 = vacation.objects.filter(Emp_name = Emp_ID, vacation_date__range = (VacStartDate, VacEndDate) ).exclude(approve_date = None).aggregate(AVnum = Count('vacation_date'))   
            elif SingleMultiUser_fun() == Profile:
                qss2 = vacation.objects.filter(Pro_name = Emp_ID, vacation_date__range = (VacStartDate, VacEndDate) ).exclude(approve_date = None).aggregate(AVnum = Count('vacation_date'))
            NumAppVac = int(qss2['AVnum'] or 0)
            apExVac =[]
            if NumAppVac > vacYear and numVac!= 0:
                numExtAppVac = NumAppVac - vacYear    #the number of day that exceed the limit number of vacation day per year 
                # print('********************')
                # print(numExtAppVac ,NumAppVac , vacYear)
                if numVac < numExtAppVac:
                    for x in range (int(numVac)):
                        apExVac.append(apVac[x])
                else:
                    for x in range (int(numExtAppVac)):
                        apExVac.append(apVac[x])
                    
            else: 
                numExtAppVac = 0    
            
            Weekend = WorkingDay.objects.filter(workDay = False)
            we=[]
            if Weekend:
                for x in Weekend:
                    we.append(x.days)

            Nvac =  NationalVacation.objects.filter(Date__month = TSMonth)
            NV=[]
            if Nvac:
                for x in Nvac:
                    NV.append(x.Date)  
            data={
                'Emp_name' :Emp_name,
                'TSMonth': TSMonth,
                'TSYear': TSYear,
                'TSDates': TSDates,
                #'TSMonth': TSDates.strftime('%B') ,
                'error_message': error_message,
                'TS_status': TS_status,
                'qs':qs,
                'apVac':apVac,
                'apExVac':apExVac,
                'weekEnd':we,
                'NVac':NV,
            }


            #today = date.today().day
            #first_month_date = date.today() - timedelta(days=(today-1))
            #next_day = TSDate + timedelta(days=30)
            #print (TSDate , today ) 
            #nowsDate = timezone.now().day #.strftime('%Y')
            #print (first_month_date)
            #print (next_day.strftime ('%A'))
            month_days=[] #define an array
            for days in range(31):
                one_day= TSDates + timedelta(days=days)
                if one_day.month == TSDates.month:
                    #print (one_day.strftime ('%d %A %B') , days) 
                    month_days.append(one_day)  #this to add new item in python. month_days[x] didn't work. 
            #print (month_days)
            context = {"form": form, 'month_days':  month_days, 'data':data }

       
    return render(request, template, context)

@login_required
def NappTsTbl_view(request):
    
    if SingleMultiUser_fun() == Employee:
        qs=  TimeSheet.objects.filter(approve_date = None).exclude(Q(submit_date = None)| Q(Emp_name =None)).values('Emp_name','Department').annotate(dcount=Count('Emp_name'))
    elif SingleMultiUser_fun() == Profile:
        user = request.user   #User.objects.get(pk = Emp_ID)
        if user.groups.filter(name="Approve Timesheet and Vacation for Self Department").exists():
            dep = Profile.objects.get(Full_name_id = user.id).Department
            qs = TimeSheet.objects.filter(approve_date = None, Pro_name__Department = dep).exclude(Q(submit_date = None) | Q(Pro_name =None)).values('Pro_name','Department').annotate(dcount=Count('Pro_name'))
        elif user.is_superuser:    
            qs=  TimeSheet.objects.filter(approve_date = None).exclude(Q(submit_date = None) | Q(Pro_name =None)).values('Pro_name','Department').annotate(dcount=Count('Pro_name'))
    if qs.exists():
        table = TSListTBL(qs)
        RequestConfig(request, paginate={'per_page': 10}).configure(table)
        context= {'table':table, 'title':'Non approve TimeSheet list' }
        
    else:
        message = 'There is no request to approve timesheet'
        context= {'title':'Non approve TimeSheet list', 'message': message }
    
    template = 'timesheets/NATSList.html'
    return render(request, template, context)

@login_required
def approveTS_view(request, emp_id):
    qs = SingleMultiUser_fun().objects.get(pk=emp_id) 
    try:
        if SingleMultiUser_fun() == Employee:
            qss = TimeSheet.objects.filter(approve_date= None, Emp_name= emp_id ).exclude(submit_date = None)
        elif SingleMultiUser_fun() == Profile:
            qss = TimeSheet.objects.filter(approve_date= None, Pro_name= emp_id ).exclude(submit_date = None)
    except:
        title= f'employee {qs} not found'
    else:
        table = ApTsTBL(qss.order_by('Date'))
        title= f'Non approve TimeSheet for emloyee: {qs} /Departement: {qs.Department} /ID= {qs.id}'
    template = 'timesheets/NATSList.html'
    context= {'table':table, 'title':title, 'NAEmpTs': True, 'qs':qs }
    if request.POST:
        status = request.POST.get('status',None)
        if status == 'approve':
            for TSrow in qss:
                if SingleMultiUser_fun() == Employee: 
                    obj , created = TimeSheet.objects.update_or_create(
                                id= TSrow.id,
                                Emp_name = TSrow.Emp_name,
                                Pro_name = None,
                                Department = TSrow.Department,
                                defaults={'approve_date':timezone.now() ,'user':request.user }
                    )
                elif SingleMultiUser_fun() == Profile:
                     obj , created = TimeSheet.objects.update_or_create(
                                id= TSrow.id,
                                Emp_name = None,
                                Pro_name = TSrow.Emp_name,
                                Department = TSrow.Department,
                                defaults={'approve_date':timezone.now() ,'user':request.user }
                    )
            message_return = "approve successfully"
            context = {'message':message_return}
            return JsonResponse(context)


        elif status == 'reject':
            for TSrow in qss:
                if SingleMultiUser_fun() == Employee:
                    obj , created = TimeSheet.objects.update_or_create(
                                id= TSrow.id,
                                Emp_name = TSrow.Emp_name,
                                Pro_name = None,
                                Department = TSrow.Department,
                                defaults={'approve_date':None ,'submit_date': None,'user':request.user }
                    )
                elif SingleMultiUser_fun() == Profile:
                    obj , created = TimeSheet.objects.update_or_create(
                                id= TSrow.id,
                                Emp_name = None,
                                Pro_name = TSrow.Emp_name,
                                Department = TSrow.Department,
                                defaults={'approve_date':None ,'submit_date': None,'user':request.user }
                    )
            message_return = "reject successfully"
            context = {'message':message_return}
            return JsonResponse(context)


    return render(request, template,context)

@login_required
def timesheetSearch_view(request,TVdate_month, TVdate_Year, emp_ID):
    if SingleMultiUser_fun() == Employee:
        qs= TimeSheet.objects.filter(Emp_name =emp_ID, Date__month = TVdate_month, Date__year = TVdate_Year )
    elif SingleMultiUser_fun() == Profile:
        qs= TimeSheet.objects.filter(Pro_name =emp_ID, Date__month = TVdate_month, Date__year = TVdate_Year )
    table = search_timesheet(qs)
    RequestConfig(request, paginate={'per_page': 10}).configure(table)
    context= {'table':table, 'title':f'TimeSheet list for employee_id:{emp_ID} month:{TVdate_month} year:{TVdate_Year}' }
    template = 'timesheets/NATSList.html'
    return render(request, template,context)
