from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

def home_view(request):
    count = User.objects.count()
    context = {'title':'authentication', 'count':count}
    return render(request, 'authen/home.html', context)

def signup_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = UserCreationForm()
    
    context = {'title':'Sign up', 'form':form}
    return render(request, 'registration/signup.html', context)

@login_required  #for test
def secure_view(request):
    return render(request, 'authen/secure.html')

class SecurePage_view(LoginRequiredMixin, TemplateView):
    template_name = 'authen/secure.html'