from django import forms
from django_select2.forms import Select2Widget
from django.forms import ModelChoiceField, widgets, SelectDateWidget, DateInput, NumberInput, TextInput
from .models import MonthlyBonusPenalty 
from employees.models import Employee, Department, Profile
from django.contrib.auth.models import User
from Salary.fun_SingleMultiUser import SingleMultiUser_fun #the system refuser to load it here as there is circular import issue
from settings.models import SingleMultiUser

class MonthlyBonusPenaltyForm(forms.Form):
    departement = forms.ModelChoiceField(
        queryset=Department.objects.all(),
        widget=Select2Widget(attrs={'data-placeholder':(u"Departement")})
        ,required = True
        )
    employee_list = forms.ModelChoiceField(
        queryset=SingleMultiUser_fun().objects.none(),   
        widget=Select2Widget(attrs={'data-placeholder':(u"Employee_Name"), 'class':'vac_emp_name'}) 
        ,required = False
        )
    bonus=  forms.FloatField(
        widget=NumberInput(attrs={'class': 'form-control '},)
        )
    penalty= forms.FloatField(
        widget=NumberInput(attrs={'class': 'form-control '},)
        )
    date= forms.DateField(
        input_formats = ['%Y-%m-%d'],
        widget = DateInput(attrs={'class': 'form-control ', 'type':'date'}, )
        ,required = True
        )
    reason= forms.CharField(
        widget = TextInput(attrs={'class': 'form-control '}, )
        )
    
    
       
    def __init__(self, *args, **kwargs): #we do this because the form.is_valid() is always False and the emp is always empty. dependent dropdown list
        super().__init__(*args, **kwargs)
    

        if 'departement' in self.data:
            try:
                Department_id= int(self.data.get('departement'))
                self.fields['employee_list'].queryset = Employee.objects.filter(Department_id=Department_id).order_by('id')
            except(ValueError,TypeError):
                pass
        elif self.instance.pk:  #there is no update in that form
            self.fields['employee_list'].queryset = self.instance.Departement.Employee_set.order_by('id')

class MonthlyBonusPenaltyFormModel(forms.ModelForm):
    #that is how to make union for the employee but we will not use it as we need all employee in one select and then to be save in 2 diferent field 
    employee_name= forms.ModelChoiceField(label= 'Employee with no TS access',queryset=Employee.objects.all().values_list('Full_name',flat=True).union(User.objects.all().values_list('username',flat=True)), widget=Select2Widget(attrs={'data-placeholder':(u"Choose one ")}))
    #that is how we can make the below field foreign key field apear in a specific order
    employee_name_Pro = forms.ModelChoiceField(label= 'Employee with TS access',queryset=Profile.objects.all().order_by('Full_name_id'), widget=Select2Widget(attrs={'data-placeholder':(u"Choose one")}))
    class Meta:
        model = MonthlyBonusPenalty
        fields = ['employee_name','employee_name_Pro', 'bonus', 'penalty', 'date', 'reason']
        exclude = ()
        widgets={
            'date' : DateInput(attrs={'class': 'form-control ', 'type':'date' },),
            # 'employee_name' :Select2Widget(attrs={'data-placeholder':(u"Employee_Name")}) ,
            # 'employee_name_Pro':Select2Widget(attrs={'data-placeholder':(u"(attrs={'data-placeholder':(u"Employee_Name")})")}) ,
            'numDay' : NumberInput (attrs={'class': 'form-control ' },),
            
        }


class MonthlyBonusPenaltyForm1(forms.Form):
    
    employee_list = forms.ModelChoiceField(
        label='Employee Name',
        queryset= SingleMultiUser_fun().objects.all().order_by('pk'),#Employee.objects.all().values_list('Full_name',flat=True).union(User.objects.all().values_list('username',flat=True).exclude(username__icontains ='admin')).exclude(Full_name__icontains ='admin'),   
        widget=Select2Widget(attrs={'data-placeholder':(u"Employee_Name")}) 
        ,required = True,
        #to_field_name="Full_name"
        )
    bonus=  forms.FloatField(
        widget=NumberInput(attrs={'class': 'form-control '},)
        ,required = False
        )
    penalty= forms.FloatField(
        widget=NumberInput(attrs={'class': 'form-control '},)
        ,required = False
        )
    date= forms.DateField(
        input_formats = ['%Y-%m-%d'],
        widget = DateInput(attrs={'class': 'form-control ', 'type':'date'}, )
        ,required = True
        )
    reason= forms.CharField(
        widget = TextInput(attrs={'class': 'form-control '}, )
        ,required = False
        )