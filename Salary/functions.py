from datetime import datetime, date, timedelta
from django.utils   import timezone
from django.db.models import Count, Sum
from django.db.models import Q , F, Value ,ExpressionWrapper, FloatField, When, Case, Window  
from django.db.models.functions import Coalesce
from datetime import date, time,timedelta
from django.db.models import Q , F, Value ,ExpressionWrapper, FloatField, Count, Sum ,Max,Min
from django.db.models import OuterRef, Subquery, DurationField

# from searches.forms import SearchFullName
from .tables import AnnualIncreaseTbl, AnnualIncreaseAppTbl, AnnualIncreaseSubTbl, MonthlyBonusPenaltyListTbl, SalaryList
from .models import AnnualIncrease, MonthlyBonusPenalty
# from .forms import MonthlyBonusPenaltyForm, MonthlyBonusPenaltyFormModel
from django.contrib.auth.models import User
from employees.models import Employee, Employee_document, Profile, Profile_document
from vacations.models import vacation
from timesheets.models import TimeSheet
from settings.models import NationalVacation, WorkingDay, TaxRate, SocialInsurance, HealthInsurance ,SingleMultiUser
from Salary.fun_SingleMultiUser import SingleMultiUser_fun
from finger_print.models import FingerPrint

#**********function to calculate detail salary **********
def singleEmpSalary(emp_id, Salary_Month):
    numDayMonth = 0
    numWeekEnd = 0
    numNationalVacation = 0
    
    #no need to define the function in the same file
    # print(SingleMultiUser_fun())
    
    #********number of day in that month
    month_days=[] #define an array to get the day of the month
    for days in range(31):
        one_day= Salary_Month + timedelta(days=days)
        if one_day.month == Salary_Month.month:
            #print (one_day.strftime ('%d %A %B %Y') , days) 
            month_days.append(one_day)
    numDayMonth = len(month_days)
    # print(numDayMonth)
    
    #*******number of weekend day in that month
    Weekend = WorkingDay.objects.filter(workDay = False)
    we=[]
    if Weekend.exists:
        for x in Weekend:
            we.append(x.days)
        #print(we)
    for x in we:
        for y in month_days:
            if x == str(y.strftime('%A')):
                numWeekEnd +=1
                #print(x, str(y.strftime('%A') ))
    # print(numWeekEnd)
    
    #*********number of national vacation in that month
    Nvac =  NationalVacation.objects.filter(Date__month = Salary_Month.month)
    z = 0  #to check if the national vacation is a weekend too in order to prevent vacation duplication
    for x in Nvac:
        for y in we:
            if str(x.Date.strftime('%A')) == y:
                z +=1
    numNationalVacation = Nvac.count() - z
    #print (Nvac, numNationalVacation)
    
    workDayMonth = numDayMonth - numWeekEnd - numNationalVacation
    workHourMonth = workDayMonth * 8
    # print(workDayMonth, workHourMonth )
    
    #**********number of hour in the approved timesheet for the employee in that month
    if SingleMultiUser_fun() == Employee:
        if SingleMultiUser_fun().objects.get(id=emp_id).TimeSheet_base == 'FingerPrint':
            qs = FingerPrint.objects.filter(
                    Date__year=Salary_Month.year, Date__month=Salary_Month.month, ROLL_NO= emp_id
                    ).order_by('ROLL_NO'
                    ).values(
                    'ROLL_NO', 'Date'
                    ).annotate(
                        WHSS= ExpressionWrapper(
                            Max('Time')-Min('Time'),output_field = DurationField()
                            ))
        else:    
            qs = TimeSheet.objects.filter(Date__year=Salary_Month.year, Date__month=Salary_Month.month, Emp_name_id= emp_id ).exclude(approve_date=None , submit_date=None).order_by('Date')
    elif SingleMultiUser_fun() == Profile:
        if SingleMultiUser_fun().objects.get(id=emp_id).TimeSheet_base == 'FingerPrint':
            qs = FingerPrint.objects.filter(
                    Date__year=Salary_Month.year, Date__month=Salary_Month.month, ROLL_NO= emp_id
                    ).order_by('ROLL_NO'
                    ).values(
                    'ROLL_NO', 'Date'
                    ).annotate(
                        WHSS= ExpressionWrapper(
                            Max('Time')-Min('Time'),output_field = DurationField()
                            ))
        else: 
            qs = TimeSheet.objects.filter(Date__year=Salary_Month.year, Date__month=Salary_Month.month, Pro_name_id= emp_id ).exclude(approve_date=None , submit_date=None).order_by('Date')
    if qs.exists():
        if SingleMultiUser_fun().objects.get(id=emp_id).TimeSheet_base == 'FingerPrint':
            qs5 = qs.aggregate(WHS = Sum('WHSS'))['WHS']
            empTSworkHour = {'WHS':int((qs5.days*24)+(qs5.seconds/3600))} 
        else: 
            empTSworkHour = qs.aggregate(WHS = Sum('working_hour'))
    else:
        empTSworkHour = {'WHS': 0} 
    # print(empTSworkHour['WHS'], qs)

    #*********actual salary of the employee after apply the approved annual increase
    qss= SingleMultiUser_fun().objects.get(id = emp_id ) 
    InitialSalary = qss.Salary
    # print(InitialSalary)
    if SingleMultiUser_fun() == Employee:
        qs1 = AnnualIncrease.objects.filter(employee_name_id = emp_id, startDate__range =(qss.Emp_date,timezone.now()) ).exclude(approveDate=None, submitDate =None).order_by('employee_name_id', 'startDate' )
    elif SingleMultiUser_fun() == Profile:
        qs1 = AnnualIncrease.objects.filter(employee_name_Pro_id = emp_id, startDate__range =(qss.Emp_date,timezone.now()) ).exclude(approveDate=None, submitDate =None).order_by('employee_name_Pro_id', 'startDate' )

    ActualSalary = InitialSalary
    for x in qs1:
        ActualSalary = float(ActualSalary or 0)
        
        ActualSalary = float(ActualSalary or 0) + (float(ActualSalary or 0)* float(x.AnnualIncreasePercent or 0)/100) + float(x.AnnualIncreaseDecimal or 0)
    # print(ActualSalary)

    #**************the sum of appoved bonus and penality in that month
    if SingleMultiUser_fun() == Employee:
        qs2 = MonthlyBonusPenalty.objects.filter(employee_name_id = emp_id ,date__month= Salary_Month.month).exclude(approveDate = None , submitDate = None)
    elif SingleMultiUser_fun() == Profile:
        qs2 = MonthlyBonusPenalty.objects.filter(employee_name_Pro_id = emp_id ,date__month= Salary_Month.month).exclude(approveDate = None , submitDate = None)

    if qs2.exists():
        for x in qs2: 
            MonthBonusPenalty = x.bonus - x.penalty
    else:
        MonthBonusPenalty = 0
    
    # print(MonthBonusPenalty) 
    
    #***********taxes for the egyptian employee 2018
    if empTSworkHour['WHS'] == 0:
        ActualTsSalary_year = 0
    else:
        ActualTsSalary_year = ((float(ActualSalary) * (float(empTSworkHour['WHS'])/float(workHourMonth))) + float(MonthBonusPenalty)) * 12.0
    qs3 = TaxRate.objects.filter(TaxStart__range =(0, ActualTsSalary_year))
    taxes= 0
    y= 0
    for x in qs3:
        if ActualTsSalary_year > x.TaxEnd:
            taxes = taxes + ((x.TaxEnd - x.TaxStart) *(x.TaxRate/100))
            y +=1
        elif ActualTsSalary_year < x.TaxEnd:
            taxes = taxes + ((ActualTsSalary_year - x.TaxStart) *(x.TaxRate/100))
            y +=1
    if y == 1 :
        taxes = taxes - 0  #taxes is already 0
    elif y == 2:
        taxes = taxes -((taxes*6/12*80/100)+(taxes*6/12*85/100))
    elif y == 3:
        taxes = taxes -((taxes*6/12*40/100)+(taxes*6/12*45/100))
    elif y == 4:
        taxes = taxes -((taxes*6/12*5/100)+(taxes*6/12*7.5/100))
    # print(y)
    taxes_month = taxes/12
    # print(taxes_month)
    
    #*********SotialInsurance for the egyptian employee 2018
    qs4 = SocialInsurance.objects.all()
    amt=[]
    for x in qs4:  #get all the amount
        amt.append(x.Amount)
    amoTotal =0
    for x in amt:  #sun the amount
        amoTotal = amoTotal + x
    companyInsuranceAm = 0
    employeeInsuranceAm = 0
    if (ActualTsSalary_year/12) >= amoTotal:
        for x in qs4:
            companyInsuranceAm = companyInsuranceAm + x.Amount *x.Company_Rate/100
            employeeInsuranceAm = employeeInsuranceAm + x.Amount *x.Employee_Rate/100
        # print(1, employeeInsuranceAm ,companyInsuranceAm)     
    elif (ActualTsSalary_year/12) < amoTotal:
        y = 0
        for x in qs4:
            y =y + x.Amount
            if y <= (ActualTsSalary_year/12):
                companyInsuranceAm = companyInsuranceAm + x.Amount *x.Company_Rate/100
                employeeInsuranceAm = employeeInsuranceAm + x.Amount *x.Employee_Rate/100
            elif y > (ActualTsSalary_year/12):
                companyInsuranceAm = companyInsuranceAm + ( (ActualTsSalary_year/12)-(y- x.Amount) ) *x.Company_Rate/100
                employeeInsuranceAm = employeeInsuranceAm + ( (ActualTsSalary_year/12)-(y- x.Amount) ) *x.Employee_Rate/100
                break
    # print(employeeInsuranceAm ,companyInsuranceAm)
    
    if ActualTsSalary_year == 0:
        Salary = 0
    else:
        Salary = ((ActualSalary) * (empTSworkHour['WHS']/workHourMonth)) + MonthBonusPenalty -taxes_month - employeeInsuranceAm #- MedicalInsurance
    
    
    salary_detail={
            'employee_id':qss.id,
            'employee_fullName': qss.Full_name,
            'employee_department': qss.Department,
            'ActualSalary':ActualSalary,
            'TotalTimeSheethour':empTSworkHour['WHS'],
            'ThismonthworkingHour': workHourMonth,
            'ThisMonthBonus_Penalty':MonthBonusPenalty,
            'MonthlyTaxes':taxes_month,
            'SocialInsurance':employeeInsuranceAm,
            'companyInsuranceAm': companyInsuranceAm,
            'ThismonthSalary': Salary
            }
    return salary_detail

        #*********end for salary detail function *******

def SingleMultiUser_fun(): #it is transfer to a separate file as this one make circular import issue with .form
    qs = SingleMultiUser.objects.filter(Single_Multi_choice = True)
    if qs.exists(): #there is any thing checked

        if qs.count() > 1: #more them 1 checked
            table = Employee
        elif qs.count() == 1: #only one checked
            qss = SingleMultiUser.objects.get(Single_Multi_choice = True)
            if qss.pk == 1:
               table = Employee
            elif qss.pk == 2:
                table = Profile

    else: #all unchecked
        table = Employee

    
    return table