# Generated by Django 2.2.3 on 2019-10-24 14:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Salary', '0007_auto_20191024_1611'),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name='annualincrease',
            name='unique_AnnualIncrease',
        ),
        migrations.AddConstraint(
            model_name='annualincrease',
            constraint=models.UniqueConstraint(fields=('employee_name', 'employee_name_Por', 'startDate'), name='unique_AnnualIncrease'),
        ),
        migrations.AddConstraint(
            model_name='monthlybonuspenalty',
            constraint=models.UniqueConstraint(fields=('employee_name', 'employee_name_Por', 'date'), name='unique_MonthlyBonusPenalty'),
        ),
    ]
