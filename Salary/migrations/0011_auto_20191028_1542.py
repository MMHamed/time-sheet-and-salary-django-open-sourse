# Generated by Django 2.2.3 on 2019-10-28 13:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Salary', '0010_auto_20191024_2023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monthlybonuspenalty',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
    ]
