from django.contrib import admin
from .models import AnnualIncrease, MonthlyBonusPenalty

admin.site.register(AnnualIncrease)
admin.site.register(MonthlyBonusPenalty)
