from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django_tables2 import RequestConfig
from datetime import datetime, date, timedelta
from django.utils   import timezone
from django.db.models import Count, Sum
from django.db.models import Q , F, Value ,ExpressionWrapper, FloatField, When, Case, Window  
from django.db.models.functions import Coalesce
from functools import reduce
from operator import add

from django.contrib.auth.models import User
from searches.forms import SearchFullName
from .tables import AnnualIncreaseTbl, AnnualIncreaseAppTbl, AnnualIncreaseSubTbl, MonthlyBonusPenaltyListTbl, SalaryList, AnnualIncreaseXTbl
from .models import AnnualIncrease, MonthlyBonusPenalty
from .forms import MonthlyBonusPenaltyForm, MonthlyBonusPenaltyFormModel, MonthlyBonusPenaltyForm1
from employees.models import Employee, Profile
from vacations.models import vacation
from timesheets.models import TimeSheet
from settings.models import NationalVacation, WorkingDay, TaxRate, SocialInsurance, HealthInsurance 
from .functions import singleEmpSalary #this is a global function for Salary to be use anyware
from Salary.fun_SingleMultiUser import SingleMultiUser_fun

    ###########updated###########
@login_required
def AnnualIncrease_view(request):
    
    if request.method =='POST':
        StDate = request.POST.get('StDate' or None)
        StDate = datetime.strptime(StDate, '%Y-%m-%d')
        qs = AnnualIncrease.objects.filter(startDate__month = StDate.month, startDate__year = StDate.year).order_by('employee_name_id','employee_name_Pro_id', 'startDate' )
        if qs.exists():
            if AnnualIncrease.objects.filter(startDate__month = StDate.month, startDate__year = StDate.year).exclude(approveDate=None).exists():
                stat = 'approved'
                table = AnnualIncreaseAppTbl(qs)
                context = {'table':table, 'title':'Annual Increase Sheet', 'StDate':StDate, 'stat':stat}
                
            else:
                stat = 'submit'
                table= AnnualIncreaseSubTbl(qs)
                
        else:
            stat = 'Not Created'
            emp = SingleMultiUser_fun().objects.all().order_by('id')
            table = AnnualIncreaseTbl(emp)
            # qs = Employee.objects.all().order_by('id')  #list all employees
            # qss = Profile.objects.all().order_by('id')
            # data =[]
            # for x in qs:
            #     data.append({'id':x.id, 'Full_name':x.Full_name, 'Department':x.Department,'Salary':x.Salary,'stat':'normal'})
            # for x in qss:
            #     data.append({'id':x.id, 'Full_name':x.Full_name, 'Department':x.Department,'Salary':x.Salary, 'stat':'extend'})
            # table = AnnualIncreaseXTbl(data)
    
    else:
        StDate = None
        stat = 'Unknown'
        emp = SingleMultiUser_fun().objects.all().order_by('id')
        table= AnnualIncreaseTbl(emp)
        # qs = Employee.objects.all().order_by('id')  #list all employees
        # qss = Profile.objects.all().order_by('id')
        # data =[]
        # for x in qs:
        #     data.append({'id':x.id, 'Full_name':x.Full_name, 'Department':x.Department,'Salary':x.Salary,'stat':'normal'})
        # for x in qss:
        #     data.append({'id':x.id, 'Full_name':x.Full_name, 'Department':x.Department,'Salary':x.Salary, 'stat':'extend'})
        # table = AnnualIncreaseXTbl(data)
    template = 'Salary/AnInc.html'
    context = {'table':table, 'title':'Annual Increase Sheet', 'StDate':StDate, 'stat':stat}
    return render(request, template, context)

   ############updated################
@login_required
def SaveAnnualIncrease_view(request):
    if request.method == 'POST':
        
        AIDate = request.POST.get('AIDate' or None)
        RowNum = request.POST.get('RowNum' or None)     
        RowNum = int(RowNum,0)
        status = request.POST.get('status' or None) 
        if status == 'Save':
            submit = timezone.now()
            approve = None
        elif status == 'Appove':
            submit = timezone.now()
            approve = timezone.now()

        for x in range(RowNum):
            list = request.POST.getlist(f'res[{x}][]' )
            if list[1] == '' or list[1]== 'None':
                list[1]= None
            if list[2] == '' or list[2]== 'None':
                list[2]= None
            if SingleMultiUser_fun() == Employee:
                obj , created = AnnualIncrease.objects.update_or_create(
                employee_name_id = list[0],
                employee_name_Pro_id = None,
                startDate = AIDate,
                defaults = {'AnnualIncreasePercent': list[1],
                            'AnnualIncreaseDecimal': list[2],
                            'submitDate' :submit,
                            'approveDate' : approve,
                            'user':request.user
                            }
                )
            elif SingleMultiUser_fun()==Profile:
                obj , created = AnnualIncrease.objects.update_or_create(
                employee_name_id = None,
                employee_name_Pro_id = list[0],
                startDate = AIDate,
                defaults = {'AnnualIncreasePercent': list[1],
                            'AnnualIncreaseDecimal': list[2],
                            'submitDate' :submit,
                            'approveDate' : approve,
                            'user':request.user
                            }
                )
            if created: 
                message_return = 'Saved successfully'
            else:
                message_return = 'Updated successfully'   
    print(message_return)
    return HttpResponse(message_return) 

############updated############
@login_required
def MonthlyBonusPenalty_view(request):
    template = 'Salary/AnInc.html'
    context = {'title':'Monthly Bonus/Penality'}
    
    if request.method == 'POST':
        form = MonthlyBonusPenaltyForm1(request.POST)
        # if form.is_valid():
        employee_name = request.POST.get('employee_list' ,None)
        
        if SingleMultiUser_fun()== Employee:
            emp = Employee.objects.get(Full_name = employee_name).id
            emp_pro = None
        elif SingleMultiUser_fun() == Profile:
            emp = None
            emp_pro = User.objects.get(username = employee_name).id
            emp_pro = Profile.objects.get(Full_name_id = emp_pro).id
            
        else:
            message ='This empoloyee is not in the database'
            print(message)
            emp = None
            emp_pro = None
        date = request.POST.get('date', None)
        bonus = request.POST.get('bonus', None)
        penalty = request.POST.get('penalty', None)
        reason = request.POST.get('reason', None)
        
        if emp != None and emp_pro != None:
            obj, created = MonthlyBonusPenalty.objects.update_or_create(
            employee_name_id= emp, 
            employee_name_Pro_id= emp_pro,
            date= date,
            defaults={'bonus': bonus, 
                    'penalty':penalty,
                    'reason':reason, 
                    'submitDate':timezone.now(), 
                    'user': request.user},
            )

            context['message'] = 'saved successfully'
        form = MonthlyBonusPenaltyForm1()
        context['form'] = form
        # else:
        #     print('form is not valid')
        #     print(form.errors)
    else:
        form = MonthlyBonusPenaltyForm1() 
        context['form']= form
    
    return render(request, template, context)

############updated###############
@login_required
def MonthlyBonusPenaltyList_view(request):
    
    if request.method == "POST":
        request_id = request.POST.get('req_id' , None)
        if 'approve' in request.POST:
            obj,created = MonthlyBonusPenalty.objects.update_or_create(
                id = request_id,
                defaults={'approveDate':timezone.now()}  
            )
        elif 'reject' in request.POST:
            obj,created = MonthlyBonusPenalty.objects.update_or_create(
                id = request_id,
                defaults={'approveDate': None , 'submitDate': None}  
            )
        # if SingleMultiUser_fun() == Employee:
        #     qs = MonthlyBonusPenalty.objects.filter(approveDate = None).exclude(Q(submitDate = None)|Q(employee_name_id= None)).order_by('employee_name_id')
        # elif SingleMultiUser_fun() == Profile:
        #     user = request.user   #User.objects.get(pk = Emp_ID)
        #     if user.groups.filter(name="Approve Timesheet and Vacation for Self Department").exists():
        #         dep = Profile.objects.get(Full_name_id = user.id).Department
        #         print(dep)
        #         qs = MonthlyBonusPenalty.objects.filter(approveDate = None, employee_name_Pro__Department = dep).exclude(Q(submitDate = None)|Q(employee_name_Pro= None)).order_by( 'employee_name_Pro')
        #     elif user.is_superuser:    
        #         qs = MonthlyBonusPenalty.objects.filter(approveDate = None).exclude(Q(submitDate = None)|Q(employee_name_Pro= None)).order_by('employee_name_id', 'employee_name_Pro')
        
        # if qs.exists():
        #     table = MonthlyBonusPenaltyListTbl(qs)
        #     template='Salary/AppBonus.html'
        #     context={'title':'Bonus/Penality need to be approved list', 'table':table}
        # else:
        #     template='Salary/AppBonus.html'
        #     context={'title':'Bonus/Penality need to be approved list', 'message':'There no bonus/penality to approve'}
    # else:
    if SingleMultiUser_fun() == Employee:
        qs = MonthlyBonusPenalty.objects.filter(approveDate = None).exclude(Q(submitDate = None)|Q(employee_name_id= None)).order_by('employee_name_id', 'employee_name_Pro')
    elif SingleMultiUser_fun() == Profile:
        user = request.user   #User.objects.get(pk = Emp_ID)
        if user.groups.filter(name="Approve Timesheet and Vacation for Self Department").exists():
            dep = Profile.objects.get(Full_name_id = user.id).Department
            qs = MonthlyBonusPenalty.objects.filter(approveDate = None, employee_name_Pro__Department = dep).exclude(Q(submitDate = None)|Q(employee_name_Pro= None)).order_by( 'employee_name_Pro')
        elif user.is_superuser:    
            qs = MonthlyBonusPenalty.objects.filter(approveDate = None).exclude(Q(submitDate = None)|Q(employee_name_Pro= None)).order_by('employee_name_id', 'employee_name_Pro')
    
    if qs.exists():
        table = MonthlyBonusPenaltyListTbl(qs)
        template='Salary/AppBonus.html'
        context={'title':'Bonus/Penality need to be approved list', 'table':table}
    else:
        template='Salary/AppBonus.html'
        context={'title':'Bonus/Penality need to be approved list', 'message':'There no bonus/penality to approve'}
    return render(request, template, context)

    
@login_required
def EmpSalaryDetail_view(request):
        
    if request.method == 'POST':
        if SingleMultiUser_fun() == Profile:
            user = request.user   #User.objects.get(pk = Emp_ID)
            if user.groups.filter(name="Self Timesheet and Vacation Request").exists():
                Emp_ID = Profile.objects.get(Full_name = user.id ).id
                #print(Emp_ID)
            else:#if the user is not in the group
                 Emp_ID = request.POST.get('employee_list',None)
        else: #if not profile
                 Emp_ID = request.POST.get('employee_list',None)

        form= SearchFullName(request.POST, None)
        TSMonth = request.POST.get('TSdate_month',None)
        TSYear = request.POST.get('TSdate_year',None)
        TSYear = int(TSYear or 0)
        TSMonth = int(TSMonth or 0)
        if Emp_ID =='' or TSMonth== '' or TSYear == '':
            context ={'title':'Salary Detail', 'form':form ,'TblLink': False, 'error_message':'You did not select the employee or the date'}
        else:
            Salary_Month= date(year= TSYear,month= TSMonth,day= 1) 
            context = singleEmpSalary(Emp_ID, Salary_Month)
            context['title'] = 'Salary Detail'
            context['form'] = form
            context['TblLink'] = False
    else:
        form= SearchFullName()
        context = {'title':'Salary Detail', 'form':form ,'TblLink': False, 'status':'please select a date and Employee'}
        context['title'] = 'Salary Detail'
        context['form'] = form
    template='Salary/Salary.html'
    return render(request, template, context)


#Salary detail linked use with the table list of salary(AllSalaryList_view)
@login_required
def EmpSalaryDetailLink_view(request, emp_id, SDate):
        
    if SDate == '0':
        context = {'title':'Salary Detail','TblLink':True, 'error_message':'You did not select a Date'}
        
    else:    
        Emp_ID = emp_id
        Salary_Month= datetime.strptime(SDate, '%Y-%m-%d %H:%M:%S')  
        context = singleEmpSalary(Emp_ID, Salary_Month)
        context['title'] = 'Salary Detail'
        context['TblLink'] = True
    template='Salary/Salary.html'
    return render(request, template, context)


@login_required
def AllSalaryList_view(request):
    
    qs = SingleMultiUser_fun().objects.all().order_by('id')
    if request.method == 'POST':
        Salary_Month = request.POST.get('SalDate', None)
        
        if Salary_Month != '':
            Salary_Month = datetime.strptime(Salary_Month, '%Y-%m-%d')
            #print(type(Salary_Month), Salary_Month )
            data =[]
            for x in qs:
                
                y = singleEmpSalary(x.pk, Salary_Month)
                
                Salary = y['ThismonthSalary']
                Insurance_Company_part = y['companyInsuranceAm'] 
                Insurance_Employee_Part  = y['SocialInsurance'] 
                taxes = y['MonthlyTaxes']
                data.append({'Employee_id':x.id,'Full_Name':x.Full_name, 'Employee_Department':x.Department,'Insurance_Company_part':Insurance_Company_part, 'Insurance_Employee_Part':Insurance_Employee_Part, 'taxes':taxes, 'Salary':Salary, 'Salary_Month':Salary_Month })
            table = SalaryList(data)
        
            context = {'table':table, 'title':'All Employee Salary List', 'status':f'For Month:{Salary_Month.month}-{Salary_Month.year}'}
        else:
            data =[]
            for x in qs:
                Salary = 0
                Insurance_Company_part = 0 
                Insurance_Employee_Part  = 0 
                taxes = 0
                Salary_Month = 0
                data.append({'Employee_id':x.id,'Full_Name':x.Full_name, 'Employee_Department':x.Department,'Insurance_Company_part':Insurance_Company_part, 'Insurance_Employee_Part':Insurance_Employee_Part, 'taxes':taxes, 'Salary':Salary , 'Salary_Month':Salary_Month})
            table = SalaryList(data)
            context = {'table':table,'title':'All Employee Salary List', 'status':f'For Month:{Salary_Month.month}-{Salary_Month.year}' }


    else:
        data =[]
        for x in qs:
            Salary = 0
            Insurance_Company_part = 0 
            Insurance_Employee_Part  = 0 
            taxes = 0
            Salary_Month = 0
            data.append({'Employee_id':x.id,'Full_Name':x.Full_name, 'Employee_Department':x.Department,'Insurance_Company_part':Insurance_Company_part, 'Insurance_Employee_Part':Insurance_Employee_Part, 'taxes':taxes, 'Salary':Salary , 'Salary_Month':Salary_Month})
        table = SalaryList(data)
        context = {'table':table,'title':'All Employee Salary List', 'status':'date not selected yet' }

    template='Salary/Salary.html'
    
    return render(request, template, context)

######this trial has fail till now = make one query to get all salary #####
@login_required
def AllSalaryList_MultiTbl_view(request):
    Salary_Month = date(year =2019, month= 10, day= 1)
    numDayMonth = 0
    numWeekEnd = 0
    numNationalVacation = 0
    
    #********number of day in that month
    month_days=[] #define an array to get the day of the month
    for days in range(31):
        one_day= Salary_Month + timedelta(days=days)
        if one_day.month == Salary_Month.month:
            #print (one_day.strftime ('%d %A %B %Y') , days) 
            month_days.append(one_day)
    numDayMonth = len(month_days)
    # print(numDayMonth)
    
    #*******number of weekend day in that month
    Weekend = WorkingDay.objects.filter(workDay = False)
    we=[]
    if Weekend.exists:
        for x in Weekend:
            we.append(x.days)
        #print(we)
    for x in we:
        for y in month_days:
            if x == str(y.strftime('%A')):
                numWeekEnd +=1
                #print(x, str(y.strftime('%A') ))
    # print(numWeekEnd)
    
    #*********number of national vacation in that month
    Nvac =  NationalVacation.objects.filter(Date__month = Salary_Month.month)
    z = 0  #to check if the national vacation is a weekend too in order to prevent vacation duplication
    for x in Nvac:
        for y in we:
            if str(x.Date.strftime('%A')) == y:
                z +=1
    numNationalVacation = Nvac.count() - z
    #print (Nvac, numNationalVacation)
    
    workDayMonth = numDayMonth - numWeekEnd - numNationalVacation
    workHourMonth = workDayMonth * 8
    # print(workDayMonth, workHourMonth )
    
    #**********number of hour in the approved timesheet for the employee in that month
    empTSworkHour = TimeSheet.objects.filter(Date__year=Salary_Month.year, Date__month=Salary_Month.month).values('Emp_name').exclude(approve_date=None , submit_date=None).order_by('Emp_name_id').annotate(WHS= Sum('working_hour'))
    
    #print(empTSworkHour, empTSworkHour[0], empTSworkHour[0]['Emp_name'], empTSworkHour[0]['WHS'])
    #work but the employee without approve time sheet will not be on the list
    
    #*********actual salary of the employee after apply the approved annual increase
    
    qs = AnnualIncrease.objects.filter(
                                startDate__lte= timezone.now()
                                ).order_by('employee_name_id', 'startDate' ).values(
                                    'employee_name_id', 'employee_name__Salary' ).exclude(
                                        approveDate=None, submitDate =None).annotate(
                                             ActualSlary = ExpressionWrapper( 
                                                                (F('employee_name__Salary')+ 
                                                                F('employee_name__Salary')*Coalesce(F('AnnualIncreasePercent'),Value(0))/Value(100))+
                                                                Coalesce(F('AnnualIncreaseDecimal'),Value(0)),
                                                                output_field=FloatField()))
    # Coalesce to Prevent an aggregate Sum() from returning None
    # we use ExpressionWrapper(...,output_field=....()) as the input table has a different tipe(in that case decimal, float...)
    #print('qs=' , qs.count(),qs[0], qs[9])

    qss = AnnualIncrease.objects.filter(
                                startDate__lte= timezone.now()
                                ).order_by('employee_name_id', 'startDate' ).values(
                                    'employee_name_id', 'employee_name__Salary' ).exclude(
                                        approveDate=None, submitDate =None).annotate(
                                             ActualSlary = Sum(
                                                                Case(
                                                                    When(
                                                                        updated__isnull = False , then = (F('employee_name__Salary')+ 
                                                                                                        F('employee_name__Salary')*Coalesce(F('AnnualIncreasePercent'),Value(0))/Value(100))+
                                                                                                        Coalesce(F('AnnualIncreaseDecimal'),Value(0))
                                                                        )
                                                                    ,default= 'employee_name__Salary', output_field = FloatField()
                                                                    )))
                                                                

    print('qss=Case/When', qss.count(), qss[0],qss[1], qss[2], qss[3])
    
     
    qss1 = AnnualIncrease.objects.filter(
                                startDate__lte= timezone.now()
                                ).order_by('employee_name_id' ).values(
                                    'employee_name_id' ).exclude(
                                        approveDate=None, submitDate =None).annotate(
                                             ActualSlary = Sum(ExpressionWrapper(
                                                                        F('employee_name__Salary')+ 
                                                                        (F('employee_name__Salary')*Coalesce(F('AnnualIncreasePercent'),Value(0))/Value(100))+
                                                                        Coalesce(F('AnnualIncreaseDecimal'),Value(0))
                                                                        ,output_field = FloatField())
                                                                    ))
    print('qss1= Sum', qss1.count(), qss1[0], qss1[9])

    qss2 = AnnualIncrease.objects.filter(
        startDate__lte= timezone.now()).order_by(
            'employee_name_id' ).exclude(
                approveDate=None, submitDate =None).values(
                    'employee_name_id','employee_name__Salary').annotate(
                        ActualSlary =  
                            ExpressionWrapper(                            
                            F('employee_name__Salary')+
                            Sum((F('employee_name__Salary')*Coalesce(F('AnnualIncreasePercent'),Value(0))/Value(100))
                                +Coalesce(F('AnnualIncreaseDecimal'),Value(0))
                            ),output_field = FloatField()
                            ))
    print('qss2= Cumulated Sum', qss2.count(), qss2[0], qss2[9])

     
    
    qss3 = AnnualIncrease.objects.filter(
        startDate__lte= timezone.now()).exclude(
                        approveDate=None, submitDate =None).annotate(
                        ActualSlary = ExpressionWrapper(                            
                                Window(Sum((F('employee_name__Salary')*((Coalesce(F('AnnualIncreasePercent'),Value(0))/Value(100))+Value(1)))
                                           +Coalesce(F('AnnualIncreaseDecimal'),Value(0))
                                           ),order_by=F('employee_name_id').asc()
                            ),output_field = FloatField()
                            )).order_by(
                                    'employee_name_id' ).values(
                                                            'employee_name_id','employee_name__Salary')
    
    print('qss3= Window Sum', qss3.count(), qss3[0],qss[1], qss3[2], qss3[3])
    
    emp_id = 1
    qs= Employee.objects.get(id = emp_id ) 
    InitialSalary =qs.Salary
    print(InitialSalary)
    qs1 = AnnualIncrease.objects.filter(employee_name_id = emp_id, startDate__range =(qs.Emp_date,timezone.now()) ).exclude(approveDate=None, submitDate =None).order_by('employee_name_id', 'startDate' )
    ActualSalary = InitialSalary
    for x in qs1:
        ActualSalary = float(ActualSalary) + (float(ActualSalary)* float(x.AnnualIncreasePercent)/100) + float(x.AnnualIncreaseDecimal)
        print(ActualSalary)

    # #**************the sum of appoved bonus and penality in that month
    # qs2 = MonthlyBonusPenalty.objects.filter(employee_name_id = emp_id ,date__month= Salary_Month.month).exclude(approveDate = None , submitDate = None)
    # if qs2.exists():
    #     for x in qs2: 
    #         MonthBonusPenalty = x.bonus - x.penalty
    # else:
    #     MonthBonusPenalty = 0
    
    # # print(MonthBonusPenalty) 
    
    # #***********taxes for the egyptian employee 2018
    # if empTSworkHour['WHS'] == 0:
    #     ActualTsSalary_year = 0
    # else:
    #     ActualTsSalary_year = ((float(ActualSalary) * (float(empTSworkHour['WHS'])/float(workHourMonth))) + float(MonthBonusPenalty)) * 12.0
    # qs3 = TaxRate.objects.filter(TaxStart__range =(0, ActualTsSalary_year))
    # taxes= 0
    # y= 0
    # for x in qs3:
    #     if ActualTsSalary_year > x.TaxEnd:
    #         taxes = taxes + ((x.TaxEnd - x.TaxStart) *(x.TaxRate/100))
    #         y +=1
    #     elif ActualTsSalary_year < x.TaxEnd:
    #         taxes = taxes + ((ActualTsSalary_year - x.TaxStart) *(x.TaxRate/100))
    #         y +=1
    # if y == 1 :
    #     taxes = taxes - 0  #taxes is already 0
    # elif y == 2:
    #     taxes = taxes -((taxes*6/12*80/100)+(taxes*6/12*85/100))
    # elif y == 3:
    #     taxes = taxes -((taxes*6/12*40/100)+(taxes*6/12*45/100))
    # elif y == 4:
    #     taxes = taxes -((taxes*6/12*5/100)+(taxes*6/12*7.5/100))
    # # print(y)
    # taxes_month = taxes/12
    # # print(taxes_month)
    
    # #*********SotialInsurance for the egyptian employee 2018
    # qs4 = SocialInsurance.objects.all()
    # amt=[]
    # for x in qs4:  #get all the amount
    #     amt.append(x.Amount)
    # amoTotal =0
    # for x in amt:  #sun the amount
    #     amoTotal = amoTotal + x
    # companyInsuranceAm = 0
    # employeeInsuranceAm = 0
    # if (ActualTsSalary_year/12) >= amoTotal:
    #     for x in qs4:
    #         companyInsuranceAm = companyInsuranceAm + x.Amount *x.Company_Rate/100
    #         employeeInsuranceAm = employeeInsuranceAm + x.Amount *x.Employee_Rate/100
                
    # elif (ActualTsSalary_year/12) < amoTotal:
    #     y = 0
    #     for x in qs4:
    #         y =y + x.Amount
    #         if y <= (ActualTsSalary_year/12):
    #             companyInsuranceAm = companyInsuranceAm + x.Amount *x.Company_Rate/100
    #             employeeInsuranceAm = employeeInsuranceAm + x.Amount *x.Employee_Rate/100
    #         elif y > (ActualTsSalary_year/12):
    #             companyInsuranceAm = companyInsuranceAm + ( (ActualTsSalary_year/12)-(y- x.Amount) ) *x.Company_Rate/100
    #             employeeInsuranceAm = employeeInsuranceAm + ( (ActualTsSalary_year/12)-(y- x.Amount) ) *x.Employee_Rate/100
    #             break
    # # print(employeeInsuranceAm ,companyInsuranceAm)
    
    # if ActualTsSalary_year == 0:
    #     Salary = 0
    # else:
    #     Salary = ((ActualSalary) * (empTSworkHour['WHS']/workHourMonth)) + MonthBonusPenalty -taxes_month - employeeInsuranceAm #- MedicalInsurance
    
    
    # salary_detail={
    #         'ActualSalary':ActualSalary,
    #         'TotalTimeSheethour':empTSworkHour['WHS'],
    #         'ThismonthworkingHour': workHourMonth,
    #         'ThisMonthBonus_Penalty':MonthBonusPenalty,
    #         'MonthlyTaxes':taxes_month,
    #         'SocialInsurance':employeeInsuranceAm,
    #         'companyInsuranceAm': companyInsuranceAm,
    #         'ThismonthSalary': Salary
    #         }
    
    # #*********end for salary detail function *******

    template='Salary/Salary.html'
    context={}
    return render(request, template, context)
