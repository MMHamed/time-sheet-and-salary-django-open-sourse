from django.db import models
from django.conf import settings #for use the AUTH.user_MODEL
from django.utils import timezone
from employees.models import Employee, Profile
from django.contrib.auth.models import User

class AnnualIncrease(models.Model):
    user  = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, null=True, on_delete=models.SET_NULL)
    employee_name = models.ForeignKey(Employee, null=True, on_delete=models.CASCADE)
    employee_name_Pro = models.ForeignKey(Profile, null=True, on_delete=models.CASCADE)
    AnnualIncreasePercent = models.FloatField(null=True, blank=True)
    AnnualIncreaseDecimal = models.FloatField(null=True, blank=True)
    startDate = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    submitDate= models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    approveDate= models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    updated = models.DateTimeField(auto_now=True)
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['employee_name','employee_name_Pro','startDate'],name='unique_AnnualIncrease')
            #each employee have only one annual increase at that date 
        ]
       # ordering = ["employee_name_id", "employee_name_Pro_id" ]
    def __str__(self):   # this to make if you query on that class, you will that object only not all the class
        return self.startDate


class MonthlyBonusPenalty(models.Model):
    user  = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    employee_name = models.ForeignKey(Employee, null=True, on_delete=models.CASCADE)
    employee_name_Pro = models.ForeignKey(Profile, null=True, on_delete=models.CASCADE)
    bonus = models.FloatField(null=True, blank =True)
    penalty = models.FloatField(null=True, blank=True)
    date = models.DateTimeField(default = timezone.now ,auto_now=False, auto_now_add=False) 
    reason = models.CharField(max_length=255,null=True, blank=True)
    submitDate= models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    approveDate= models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    updated = models.DateTimeField(auto_now=True)
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['employee_name','employee_name_Pro','date'],name='unique_MonthlyBonusPenalty')
            #each employee have only one bonus at that date 
        ]

    def __str__(self):   # this to make if you query on that class, you will that object only not all the class
        return self.date
