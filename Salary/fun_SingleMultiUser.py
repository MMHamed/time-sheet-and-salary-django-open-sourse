from django.core.exceptions import ObjectDoesNotExist
from employees.models import Employee, Employee_document, Profile, Profile_document
from settings.models import SingleMultiUser


def SingleMultiUser_fun():  # it is transfer to a separet file as this one make circular import issue with .form
    try:
        qs = SingleMultiUser.objects.get(Single_Multi_choice=True)
    except:  # all unchecked or all checked or any error
        table = Employee

    else:
        # there is any thing checked
        qss = SingleMultiUser.objects.filter(Single_Multi_choice=True)
        if qss.count() > 1:  # more them 1 checked
            table = Employee
        elif qss.count() == 1:  # only one checked
            if qs.pk == 1:
                table = Employee
            elif qs.pk == 2:
                table = Profile

    return table
