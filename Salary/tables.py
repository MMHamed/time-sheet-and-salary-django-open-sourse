import django_tables2 as tables
from django import forms
import itertools
from django_tables2.utils import A
from django.utils.safestring import mark_safe 
from django.utils.html import format_html
from datetime import datetime
from django.utils   import timezone
from django.views.decorators import csrf
from django.middleware.csrf import get_token

from .models import AnnualIncrease, MonthlyBonusPenalty
from employees.models import Employee, Profile
from Salary.fun_SingleMultiUser import SingleMultiUser_fun

TEMPLATE ="""
 <div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="Percent of the Salary"  style='width:10px;'>
  <div class="input-group-append">
    <span class="input-group-text" id="basic-addon2">%</span>
  </div>
</div>
"""
TEMPLATE1="""
<div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="fixed Money"  style='width:10px;'>
  <div class="input-group-append">
    <span class="input-group-text" id="basic-addon2">$</span>
  </div>
</div>
"""

TEMPLATE2 ="""    
<select maxlenght='10' name='Working_Hour' class="btn btn-outline-primary Working_Hour" data-placeholder='choose one' disabled= 'disabled'>
    <option value=''></option>
    <option value='0' id='0'>absent</option>
    <option value='4' id='4'>Half_day</option>
    <option value='8' id='8'>Full_day</option>
</select>

"""

class AnnualIncreaseTbl(tables.Table):
    Percent_Increase = tables.TemplateColumn(TEMPLATE, orderable=False) #work to add an input column to the table!!
    Fixed_Increase = tables.TemplateColumn(TEMPLATE1, orderable=False)
    #New_Salary = tables.Column(empty_values=())
    row_number = tables.Column(empty_values=(), verbose_name='#')   #to add a row that increament each row->it work
    Salary = tables.Column(verbose_name = 'Old Salary')
    class Meta:
        model= Employee
        fields= ['id', 'Full_name', 'Department' ]
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'id','Department', 'Full_name','Salary','Percent_Increase','Fixed_Increase'  ]
        orderable = False  #to disable orderable in the hole table
    
    def __init__(self, *args, **kwargs):
        super(AnnualIncreaseTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return 'Row %d' % next(self.counter)

    def render_Salary(self, value, record):
        #qs = Employee.objects.get(id = record['id'])
        InSalary = record.Salary
        qss= AnnualIncrease.objects.filter(employee_name_id = record.id, startDate__lte = timezone.now() ).exclude(approveDate = None)
        if qss.exists():
            actSlary = float(InSalary)
            for x in qss:
                actSlary = actSlary + (actSlary* x.AnnualIncreasePercent/100) + x.AnnualIncreaseDecimal 

        else:
            actSlary = float(InSalary) 
        return '%f' %actSlary


class AnnualIncreaseXTbl(tables.Table):
    Percent_Increase = tables.TemplateColumn(TEMPLATE, orderable=False) #work to add an input column to the table!!
    Fixed_Increase = tables.TemplateColumn(TEMPLATE1, orderable=False)
    row_number = tables.Column(empty_values=(), verbose_name='#')   #to add a row that increament each row->it work
    Salary = tables.Column(verbose_name = 'Old Salary')
    id = tables.Column()
    Full_name = tables.Column()
    Department= tables.Column()
    stat = tables.Column(attrs={'th':{'style':'display:none;'},
                                'td':{'style':'display:none;'}
                                    })
    class Meta:
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'id','Department', 'Full_name','Salary','Percent_Increase','Fixed_Increase'  ]
        orderable = False  #to disable orderable in the hole table
    
    def __init__(self, *args, **kwargs):
        super(AnnualIncreaseXTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return 'Row %d' % next(self.counter)

    def render_Salary(self, value, record):
        #qs = Employee.objects.get(id = record['id'])
        InSalary = value
        qss= AnnualIncrease.objects.filter(employee_name_id = record['id'], startDate__lte = timezone.now() ).exclude(approveDate = None)
        if qss.exists():
            actSlary = float(InSalary)
            for x in qss:
                actSlary = actSlary + (actSlary* x.AnnualIncreasePercent/100) + x.AnnualIncreaseDecimal 

        else:
            actSlary = float(InSalary) 
        return '%f' %actSlary

class AnnualIncreaseSubTbl(tables.Table):
    Percent_Increase = tables.TemplateColumn(TEMPLATE, orderable=False) #work to add an input column to the table!!
    Fixed_Increase = tables.TemplateColumn(TEMPLATE1, orderable=False)
    New_Salary = tables.Column(empty_values=())
    row_number = tables.Column(empty_values=(), verbose_name='#')   #to add a row that increament each row->it work
    Old_Salary = tables.Column(empty_values=())
    Department = tables.Column(empty_values=())
    id = tables.Column(empty_values=())
    stat = tables.Column(empty_values=(), attrs={'th':{'style':'display:none;'},
                                                'td':{'style':'display:none;'}
                                    })
    employee_name = tables.Column(empty_values=())
    # employee_name_Pro=tables.Column(attrs={'th':{'style':'display:none;'},
    #                                        'td':{'style':'display:none;'}
    #                                 })
    class Meta:
        model= AnnualIncrease
        fields= ['employee_name' ]
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'id','Department', 'employee_name','Old_Salary','Percent_Increase','Fixed_Increase', 'New_Salary'  ]
        orderable = False  #to disable orderable in the hole table
    
    def __init__(self, *args, **kwargs):
        super(AnnualIncreaseSubTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return '%d' % next(self.counter)

    def render_employee_name(self,value,record):
        if record.employee_name:
            return '%s' % record.employee_name
        elif record.employee_name_Pro:
            return '%s' % record.employee_name_Pro

    def render_Old_Salary(self, value, record):
        if record.employee_name:
            qs= Employee.objects.get(pk = record.employee_name_id)
            qss= AnnualIncrease.objects.filter(employee_name_id = record.employee_name_id ).exclude(approveDate = None)

        elif record.employee_name_Pro:
            qs= Profile.objects.get(pk = record.employee_name_Pro_id)
            qss= AnnualIncrease.objects.filter(employee_name_Pro_id = record.employee_name_Pro_id ).exclude(approveDate = None)

        InSalary = qs.Salary
        if qss.exists():
            actSlary = float(InSalary or 0)
            num = len(qss)-1 #that to add all the encrease except the last one
            y = 0
            if num > y:  #if len is not equal to  1
                for x in qss:
                    actSlary = actSlary + (actSlary* x.AnnualIncreasePercent/100) + x.AnnualIncreaseDecimal 
                    y += 1
                    if num == y:
                        break
        else:
            actSlary = float(InSalary or 0)
        return '%f' %actSlary

    def render_New_Salary(self, value, record):
        if record.employee_name:
            qs= Employee.objects.get(id = record.employee_name_id)
            qss= AnnualIncrease.objects.filter(employee_name_id = record.employee_name_id )
        elif record.employee_name_Pro:
            qs= Profile.objects.get(id = record.employee_name_Pro_id)
            qss= AnnualIncrease.objects.filter(employee_name_Pro_id = record.employee_name_Pro_id )
            
        InSalary = qs.Salary
        
        if qss.exists():
            actSlary = float(InSalary or 0)
            
            for x in qss:
                if x.AnnualIncreasePercent == None:
                    x.AnnualIncreasePercent = 0
                if x.AnnualIncreaseDecimal == None:
                    x.AnnualIncreaseDecimal = 0
                actSlary = float(actSlary or 0) + (actSlary* x.AnnualIncreasePercent/100) + x.AnnualIncreaseDecimal 

        else:
            actSlary = float(InSalary or 0) 
        return '%f' %actSlary

    def render_Department(self, value, record):
        if record.employee_name:
            qs= Employee.objects.get(id = record.employee_name_id)
            return '%s' %qs.Department
        elif record.employee_name_Pro:
            qs= Profile.objects.get(id = record.employee_name_Pro_id)
            return '%s' %qs.Department
        
    
    def render_Percent_Increase(self, value, record):
        return format_html(" {} value = '{}' {}" ,
                mark_safe ('<div class="input-group mb-3"><input type="text" class="form-control" placeholder="Percent of the Salary" '),
                record.AnnualIncreasePercent,
                mark_safe('style="width:20px;"><div class="input-group-append"><span class="input-group-text" id="basic-addon2">%</span></div></div>')
                          )
                        

    
    def render_Fixed_Increase(self, value, record):
        return format_html(" {} value = '{}' {}" ,
                mark_safe ('<div class="input-group mb-3"><input type="text" class="form-control" placeholder="Percent of the Salary" '),
                record.AnnualIncreaseDecimal,
                mark_safe('style="width:60px;"><div class="input-group-append"><span class="input-group-text" id="basic-addon2">$</span></div></div>')
                          )
        

    def render_id(self, value, record):
        if record.employee_name:
            return '%s' %record.employee_name_id
        elif record.employee_name_Pro:
             return '%s' %record.employee_name_Pro_id

    def render_stat(self, value, record):
        if record.employee_name:
            return '%s' %'normal'
        elif record.employee_name_Pro:
             return '%s' %'extend'

class AnnualIncreaseAppTbl(tables.Table):
    New_Salary = tables.Column(empty_values=())
    Department= tables.Column(empty_values=())
    row_number = tables.Column(empty_values=(), verbose_name='#')   #to add a row that increament each row->it work
    Old_Salary = tables.Column(empty_values=())
    AnnualIncreasePercent = tables.Column(verbose_name = 'Percent_Increase')
    AnnualIncreaseDecimal = tables.Column(verbose_name = 'Fixed_Increase')
    employee_name_id = tables.Column(verbose_name = 'id')
    class Meta:
        model= AnnualIncrease
        fields= ['employee_name_id', 'employee_name', 'AnnualIncreasePercent', 'AnnualIncreaseDecimal' ]
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'employee_name_id','Department', 'employee_name','Old_Salary','AnnualIncreasePercent','AnnualIncreaseDecimal', 'New_Salary'  ]
        orderable = False  #to disable orderable in the hole table
    
    def __init__(self, *args, **kwargs):
        super(AnnualIncreaseAppTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return '%d' % next(self.counter)

    def render_Old_Salary(self, value, record):
        if SingleMultiUser_fun() == Employee:
            qs = SingleMultiUser_fun().objects.get(id = record.employee_name_id)
            qss= AnnualIncrease.objects.filter(employee_name_id = record.employee_name_id ).exclude(approveDate = None).order_by('startDate')
        elif SingleMultiUser_fun() == Profile:
            qs = SingleMultiUser_fun().objects.get(id = record.employee_name_id)
            qss= AnnualIncrease.objects.filter(employee_name_Pro_id = record.employee_name_Pro_id ).exclude(approveDate = None).order_by('startDate')
        InSalary = qs.Salary
        if qss.exists():
            actSlary = float(InSalary)
            num = len(qss)-1 #that to add all the encrease except the last one
            y = 0
            if num > y:
                for x in qss:
                    actSlary = actSlary + (actSlary* x.AnnualIncreasePercent/100) + x.AnnualIncreaseDecimal 
                    y += 1
                    if num == y:
                        break
                   
        else:
            actSlary = float(InSalary) 
        return '%f' %actSlary

    def render_Department(self, value, record):
        if SingleMultiUser_fun() == Employee:
            qs= SingleMultiUser_fun().objects.get(pk = record.employee_name_id)
        elif SingleMultiUser_fun() == Profile:
            qs= SingleMultiUser_fun().objects.get(pk = record.employee_name_Pro_id)
        return '%s' %qs.Department
    
    def render_New_Salary(self, value, record):
        if SingleMultiUser_fun() == Employee:
            qs= SingleMultiUser_fun().objects.get(pk = record.employee_name_id)
            qss= AnnualIncrease.objects.filter(employee_name_id = record.employee_name_id ).exclude(approveDate = None)
        elif SingleMultiUser_fun() == Profile:
            qs= SingleMultiUser_fun().objects.get(pk = record.employee_name_Pro_id)
            qss= AnnualIncrease.objects.filter(employee_name_Pro_id = record.employee_name_Pro_id ).exclude(approveDate = None)
        InSalary = qs.Salary
        
        
        if qss.exists():
            actSlary = float(InSalary)
            for x in qss:
                actSlary = actSlary + (actSlary* x.AnnualIncreasePercent/100) + x.AnnualIncreaseDecimal 
        else:
            actSlary = float(InSalary) 
        return '%f' %actSlary

class MonthlyBonusPenaltyListTbl(tables.Table):
    row_number = tables.Column(empty_values=(), verbose_name='#')
    Decision = tables.Column(empty_values=(), )
    Department = tables.Column(empty_values=())
    employee_name = tables.Column(empty_values=() )
    id= tables.Column(verbose_name='Request ID')
    date = tables.Column(verbose_name='Request Date')
    class Meta:
        model= MonthlyBonusPenalty
        fields= ['id','employee_name', 'bonus', 'penalty','date', 'reason','submitDate' ]
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'id','Department', 'employee_name', 'bonus', 'penalty','date', 'reason','submitDate' ,'Decision']
        orderable = False  #to disable orderable in the hole table
    
    def __init__(self, *args, **kwargs):
        super(MonthlyBonusPenaltyListTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return '%d' % next(self.counter)

    def render_Department(self, value, record):
        if SingleMultiUser_fun() == Employee:
            qs= SingleMultiUser_fun().objects.get(pk = record.employee_name_id)
        elif SingleMultiUser_fun() == Profile:
            qs= SingleMultiUser_fun().objects.get(pk = record.employee_name_Pro_id)
        return '%s' %qs.Department

    def render_employee_name(self, value, record):
        if SingleMultiUser_fun() == Employee:
            qs= SingleMultiUser_fun().objects.get(pk = record.employee_name_id)
        elif SingleMultiUser_fun() == Profile:
            qs= SingleMultiUser_fun().objects.get(pk = record.employee_name_Pro_id)
        return '%s' %qs


    def render_Decision(self, value, record):
        csrf_token = get_token(self.context["request"])
        return format_html(" {} {} value='{}' {} value= '{}' {}" ,
                mark_safe ('<form method ="POST"> '),
                mark_safe('<input type="hidden" name="csrfmiddlewaretoken"  '),
                format(csrf_token),
                mark_safe ('/><input type="hidden" name="req_id"  '),
                record.id,
                mark_safe('><div class="btn-group btn-group-sm" role="group" aria-label="Basic example"><button type="submit" name="approve" class="btn btn-outline-primary">Appove</button><button type="submit" name="reject" class="btn btn-outline-secondary">Reject</button></div></form>')
                          ) 

class SalaryList(tables.Table):
     
    # row_number = tables.Column(empty_values=(), verbose_name='#')
    
    Employee_id = tables.Column()
    Full_Name = tables.LinkColumn('EmpSalaryDetailLink', args=[A('Employee_id'), A('Salary_Month') ])
    Employee_Department = tables.Column(footer=lambda table: 'TOTALS:')
    Insurance_Company_part= tables.Column(footer=lambda table: sum(x["Insurance_Company_part"] for x in table.data))
    Insurance_Employee_Part= tables.Column(footer=lambda table: sum(x["Insurance_Employee_Part"] for x in table.data))
    taxes= tables.Column(footer=lambda table: sum(x["taxes"] for x in table.data))
    Salary = tables.Column(footer=lambda table: sum(x["Salary"] for x in table.data))
    Salary_Month = tables.Column(
                                attrs={'th':{'style':'display:none;'},
                                        'td':{'style':'display:none;'}
                                    })
    class Meta:
        template_name = "django_tables2/semantic.html"
        orderable = False
        # attrs={'th':{'style':'border: 1px solid black;'},   #work
        #        'td':{'style':'border: 1px solid black;'}
        #                 }
    
    def __init__(self, *args, **kwargs):
        super(SalaryList, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    # def render_row_number(self):
    #     return '%d' % next(self.counter)