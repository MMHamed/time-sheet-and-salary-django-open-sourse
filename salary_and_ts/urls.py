"""salary_and_ts URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from searches.views import search_view, load_emp
from timesheets.views import GrpTimesheet_view, timesheet_save, Sgltimesheet_view, NappTsTbl_view, approveTS_view, timesheetSearch_view
from vacations.views import vacation_view, vacation_apporveList_view, vacation_apporve_view, vacationSearch_view
from settings.views import NaVaction_view, Workingday_view, OverTime_view, TravelAllowance_view, TaxRate_view, SocialInsurance_view, HealthInsurance_view, Shifts_view, SingleMultiUser_view, Department_view
from authen.views import home_view, signup_view, secure_view, SecurePage_view
from Salary.views import AnnualIncrease_view, SaveAnnualIncrease_view, MonthlyBonusPenalty_view, MonthlyBonusPenaltyList_view, EmpSalaryDetail_view, AllSalaryList_view, AllSalaryList_MultiTbl_view, EmpSalaryDetailLink_view
from finger_print.views import export_view, import_view 
urlpatterns = [
    # authen
    path('',home_view, name='home'),
    path('secure',secure_view, name='secure'),
    path('secure2', SecurePage_view.as_view(), name='secure2'),
    path('signup/',signup_view, name='signup'),
    path('accounts/', include('django.contrib.auth.urls')),
    #employees
    path('emp/',include('employees.urls')),
    #timesheets
    path('GrpTimesheet/', GrpTimesheet_view),
    path('ajax/save_ts/',timesheet_save, name='saveTimesheet'),
    path('SglTimesheet/', Sgltimesheet_view),
    path('NappTsTbl/', NappTsTbl_view),
    path('<int:emp_id>/approveTS/',approveTS_view, name='approveTimesheet'),
    path('timesheet/<int:TVdate_month>/<int:TVdate_Year>/<int:emp_ID>',timesheetSearch_view ),
    #vacations
    path('vacationForm/', vacation_view),
    path('vacationApproveList/', vacation_apporveList_view),
    path('<int:emp_id>/<int:req_num>/vacationApprove/',vacation_apporve_view, name ='approveVacation' ),
    path('vacation/<int:TVdate_Year>/<int:emp_ID>', vacationSearch_view),
    #settings
    path('NaVaction/',NaVaction_view),
    path('WorkDay/',Workingday_view),
    path('SingleMultiUser/',SingleMultiUser_view),
    path('OverTime/',OverTime_view),
    path('TravelAllowance/',TravelAllowance_view), 
    path('TaxRate/',TaxRate_view), 
    path('SocialInsurance/',SocialInsurance_view), 
    path('HealthInsurance/',HealthInsurance_view), 
    path('Shifts/',Shifts_view),
    path('Department/', Department_view),
    #search
    path('search/', search_view),
    path('ajax/load-emp/', load_emp , name='ajax_load_emp'),
    #salary
    path('AnInc/',AnnualIncrease_view),
    path('SaveAnInc/',SaveAnnualIncrease_view),
    path('MoBoPe/',MonthlyBonusPenalty_view),
    path('tblBonus/',MonthlyBonusPenaltyList_view, name='Bonus_Table'),
    path('EmpSalDet/',EmpSalaryDetail_view, name = 'EmpSalaryDetail'),
    path('<int:emp_id>/<str:SDate>/EmpSalDetLink/', EmpSalaryDetailLink_view, name= 'EmpSalaryDetailLink'),
    path('EmpSalList/',AllSalaryList_view, name ='AllSalaryList'),
    path('EmpSalListMT/',AllSalaryList_MultiTbl_view, name='AllSalaryList_MultiTbl'),
    #finger_print
    path('import/',import_view),
    path('export/',export_view, name= 'export'),
    #select2
    path('select2/', include('django_select2.urls')),
    #admin
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    #test mode not live
    from django.conf.urls.static import static
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)