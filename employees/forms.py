from django import forms
from django_select2.forms import Select2Widget
from .models import Employee, Employee_document, Profile, Profile_document
from django.contrib.auth.models import User
from Salary.fun_SingleMultiUser import SingleMultiUser_fun


class DateInput(forms.DateInput):
    input_type = 'date'

class EmployeeModelForm(forms.ModelForm):
    if SingleMultiUser_fun() == Employee:
        class Meta:
            model= Employee
            fields = ['Full_name','National_id', 'Address', 'Department','Emp_date','Salary_base','Salary','TimeSheet_base']
            widgets={
                'Emp_date':DateInput(),
                'Department': Select2Widget,
                'Salary_base': Select2Widget,
                'TimeSheet_base': Select2Widget,
            }

    elif SingleMultiUser_fun() == Profile:
        class Meta:
                model= Profile
                fields = ['National_id', 'Address', 'Department','Emp_date','Salary_base','Salary','TimeSheet_base']
                widgets={
                    'Emp_date':DateInput(),
                    'Department': Select2Widget,
                    'Salary_base': Select2Widget,
                    'TimeSheet_base': Select2Widget,
                }

class UserModelForm(forms.ModelForm):
    class Meta:
        model= User
        exclude = ['last_login', 'is_superuser', 'groups', 'user_permissions', 'is_staff', 'date_joined']

class EmployeeDocModelForm(forms.ModelForm):
    if SingleMultiUser_fun() == Employee:
        class Meta:
            model= Profile_document
            fields = ['Photo']

    elif SingleMultiUser_fun() == Profile:
        class Meta:
            model= Profile_document
            fields = ['Photo']

class ProfileModelForm(forms.ModelForm):
    class Meta:
        model= Profile
        fields = ['Full_name','National_id', 'Address', 'Department','Emp_date','Salary_base','Salary','TimeSheet_base']
        widgets={
            'Emp_date':DateInput(),
            'Department': Select2Widget,
            'Salary_base': Select2Widget,
            'TimeSheet_base': Select2Widget,
        }

class ProfileDocModelForm(forms.ModelForm):
    class Meta:
        model= Profile_document
        fields = ['Photo']
        