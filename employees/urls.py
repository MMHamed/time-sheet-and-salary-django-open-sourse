from django.urls import path
from . import views

urlpatterns=[
    path('', views.index, name='index'),
    path('list/',views.employee_list, name='employee_list'),
    path('table/',views.employee_tbl, name='employee_Table'),
    path('<int:emp_id>/detail',views.employee_detail, name='employee_detail'),
    path('emp_add/',views.employee_add, name='employee_add'),
    path('<int:emp_id>/edit',views.employee_update,name='employee_update'),
]