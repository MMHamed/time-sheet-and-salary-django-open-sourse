import django_tables2 as tables
from django import forms
import itertools
from django_tables2.utils import A
from .models import Employee, Profile
from Salary.fun_SingleMultiUser import SingleMultiUser_fun

class EmpTable(tables.Table):
    row_number = tables.Column(empty_values=())   #to add a row that increament each row->it work
    Full_name = tables.LinkColumn('employee_detail', args=[A('pk')]) #to make a row on click go to detail-emp
    class Meta:
        model= SingleMultiUser_fun()
        fields= ['id', 'Full_name', 'Department' ]
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'id', 'Full_name', 'Department']

        row_number=tables.Column(empty_values=())   #to add a row that increament each row->it work

    def __init__(self, *args, **kwargs):
        super(EmpTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return 'Row %d' % next(self.counter)

class EmpxTable(tables.Table):
    row_number = tables.Column(empty_values=())   #to add a row that increament each row->it work
    id = tables.Column()
    Full_name = tables.LinkColumn('employee_detail', args=[A('id'),A('stat')])
    Department= tables.Column()
    stat = tables.Column(attrs={'th':{'style':'display:none;'},
                                'td':{'style':'display:none;'}
                                    })
    class Meta:
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'id', 'Full_name', 'Department','stat']

        

    def __init__(self, *args, **kwargs):
        super(EmpxTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return 'Row %d' % next(self.counter)