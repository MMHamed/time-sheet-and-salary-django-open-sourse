# Generated by Django 2.2.3 on 2019-07-25 16:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('employees', '0011_employee_updated'),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee_extra',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Photo', models.ImageField(blank=True, null=True, upload_to='image/')),
                ('Emp_name', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='employees.Employee')),
            ],
        ),
    ]
