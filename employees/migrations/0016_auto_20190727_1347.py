# Generated by Django 2.2.3 on 2019-07-27 11:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('employees', '0015_auto_20190726_2309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee_document',
            name='Emp_name',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='employees.Employee'),
        ),
    ]
