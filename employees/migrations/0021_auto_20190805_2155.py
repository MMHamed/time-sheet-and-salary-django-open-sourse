# Generated by Django 2.2.3 on 2019-08-05 19:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employees', '0020_auto_20190805_2008'),
    ]

    operations = [
        migrations.AlterField(
            model_name='department',
            name='Dep_name',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='employee',
            name='Salary_base',
            field=models.CharField(choices=[('Monthly', 'Monthly-base'), ('Hourly', 'Hourly-base')], default='Monthly', max_length=200),
        ),
    ]
