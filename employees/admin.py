from django.contrib import admin

# Register your models here.
from .models import Department , Employee, Employee_document

admin.site.register(Department)
admin.site.register(Employee)
admin.site.register(Employee_document)
