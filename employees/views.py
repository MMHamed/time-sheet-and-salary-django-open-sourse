from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render,redirect, get_object_or_404
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django_tables2 import RequestConfig
from django.utils   import timezone
from datetime import date, time,timedelta
from django.db.models import Q , F, Value ,ExpressionWrapper, FloatField, Count, Sum ,Max,Min
from django.db.models import OuterRef, Subquery, DurationField

from django.contrib.auth.models import User
from .models import Employee, Employee_document, Profile, Profile_document
from .forms import EmployeeModelForm, EmployeeDocModelForm, ProfileModelForm, ProfileDocModelForm, UserModelForm
from .tables import EmpTable, EmpxTable
from Salary.fun_SingleMultiUser import SingleMultiUser_fun
# from finger_print.models import FingerPrint

@login_required
def index(request):
    # #this to calculate the delta second between the first access and the last access in that a day# it work
    
    # print('************') 
    # emp_id=1
    # Salary_Month = date(2018,11,1)
    # qs = FingerPrint.objects.filter(
    #     Date__month = timezone.now().month ).order_by('ROLL_NO'
    #     ).values(
    #         'ROLL_NO', 'Date'
    #         ).annotate(
    #          hourPerDay= ExpressionWrapper(
    #             Max('Time')-Min('Time'),output_field = DurationField()
    #              ))
    # qs2 = SingleMultiUser_fun().objects.get(id=emp_id).TimeSheet_base
    # qs1 = FingerPrint.objects.filter(
    #                 Date__year=Salary_Month.year, Date__month=Salary_Month.month, ROLL_NO= emp_id
    #                 ).order_by('ROLL_NO'
    #                 ).values(
    #                 'ROLL_NO', 'Date'
    #                 ).annotate(
    #                     WHSS= ExpressionWrapper(
    #                         Max('Time')-Min('Time'),output_field = DurationField()
    #                         ))
    # print(qs)
    # print(qs2)
    # print(qs1)
    # qs3= qs1.aggregate(WHS = Sum('WHSS'))['WHS']
    # print(qs3)
    # print(int((qs3.days*24)+(qs3.seconds/3600)+0.1))
    # # for x in qs1:
    #     print ('WHSS=',x['WHSS'])
    #     for y in x:
    #         print(y)
    # print(emp_id, Salary_Month)
    context ={}
    return render (request,'employees/test.html', context)

@login_required
def employee_list(request):
    qs = SingleMultiUser_fun().objects.all().order_by('pk')  #list all employees
    #qss = Profile.objects.all().order_by('pk') 
    print(qs)
    template_name = 'employees/list.html'
    context = {'object_list':qs, 'title':'List of emplyees', 'list':'True'}
    
    if not qs:
        return HttpResponse("no employee available" )
    return render(request, template_name, context)

@login_required
def employee_tbl(request):
    #table = EmpTable(Employee.objects.all())
    qs = SingleMultiUser_fun().objects.all().order_by('pk')  #list all employees
    # qss = Profile.objects.all().order_by('pk')
    # data =[]
    # for x in qs:
    #     data.append({'id':x.pk, 'Full_name':x.Full_name, 'Department':x.Department,'stat':'normal'})
    # for x in qss:
    #     data.append({'id':x.pk, 'Full_name':x.Full_name, 'Department':x.Department,'stat':'extend'})
    #table = EmpxTable(data)
    table= EmpTable(qs)
    RequestConfig(request, paginate={'per_page': 10}).configure(table)
    context= {'table':table, 'title':'Empl_in_table' }
    return render(request, 'employees/list.html', context)


@login_required
def employee_detail(request, emp_id):
           
    qs = SingleMultiUser_fun().objects.get(pk = emp_id)
    # qs1 = qs.Employee_document_set.first() #to query about employee related employee_document
    try:
        if SingleMultiUser_fun() == Employee:
            qs1 = qs.employee_document #_set.first() for many to many relationship
            
        elif SingleMultiUser_fun() == Profile:
            qs1 = qs.profile_document #_set.first() 
                 
        
    except ObjectDoesNotExist:  
        context = {'object_list': qs, 'error_message' : "Photo not uploaded yet", 'title':f'detail of Employee:{qs.Full_name}', 'detail':'True'}
            
    else:
        qs1 = qs1.Photo.url
        context = {'object_list': qs, 'object_list1': qs1, 'title':f'detail of Employee:{qs.Full_name}', 'detail':'True'}
        
    template_name = 'employees/list.html'
    
    return render(request,template_name,context)

@staff_member_required
@login_required
def employee_add(request):
    if request.method == 'Post':
        form = EmployeeModelForm(request.POST or None)
        form1 = EmployeeDocModelForm(request.POST or None, request.FILES or None)
        form2= UserModelForm(request.POST or None)
        if form2.is_valid():
            obj=form2.save()
        if form.is_valid():
            #print(f"form= {form.cleaned_data}")
            #print(f"user={request.user}")
            obj=form.save(commit=False)
            obj.user = request.user
            obj.save()
        if form1.is_valid():
            #print(f"form1= {form1.cleaned_data}")
            obj1=form1.save(commit=False)
            obj1.Emp_name = obj
            obj1.save()
            #obj1 = obj.employee_document_set.create(form1)

        if 'home' in request.POST:  #this is button name in the form multi button
            # print("home")
            return redirect("/emp/list")
        elif 'update' in request.POST:  # this is button name in the form multi button
            # print('update')
            return redirect (f"/emp/{obj.id}/edit")
        elif 'addNew' in request.POST:  #this is button name in the form multi button
            # print("add new")
            form = EmployeeModelForm()
            form1 = EmployeeDocModelForm()
            form2= UserModelForm()
    else:
        form = EmployeeModelForm()
        form1 = EmployeeDocModelForm()
        form2= UserModelForm()

    if SingleMultiUser_fun() == Profile:
        context= {"form2":form2,"form":form, "form1": form1 , "title":"ADD NEW Employee"}
    else:
        context= {"form":form, "form1": form1 , "title":"ADD NEW Employee"}

    template_name = 'employees/form.html'
    return render(request,template_name,context)

@staff_member_required
@login_required
def employee_update(request, emp_id):
    if SingleMultiUser_fun() == Employee:
        qss = Employee
        qss1= Employee_document
        qss2= EmployeeModelForm
        qss3= EmployeeDocModelForm
    elif SingleMultiUser_fun() == Profile:
        qss = Profile
        qss1= Profile_document
        qss2= ProfileModelForm
        qss3= ProfileDocModelForm

    qs = get_object_or_404(qss, pk=emp_id)
    
    try:
      qs1 = qss1.objects.get(Emp_name=emp_id)
    
    except(KeyError, qss1.DoesNotExist):
        obj = qss2(request.POST or None, instance=qs)
        if obj.is_valid():
            obj.save()
        
        obj1 = qss3(request.POST or None, request.FILES or None)
        if obj1.is_valid():
            obj11 = obj1.save(commit=False)
            obj11.Emp_name = qs
            obj11.save()

            

    else:
        obj = qss2(request.POST or None, instance=qs)
        if obj.is_valid():
            obj.save()

        obj1 = qss3(request.POST or None, request.FILES or None, instance=qs1)
        if obj1.is_valid():
            obj1.save()

    if 'home' in request.POST:  #this is button name in the form multi button
        return redirect("/emp/list")

    elif 'update' in request.POST:  # this is button name in the form multi button
        return redirect (f"/emp/{emp_id}/edit")

    elif 'addNew' in request.POST:  #this is button name in the form multi button
        obj = EmployeeModelForm()
        obj1 = EmployeeDocModelForm()
        return redirect ('/emp/emp_add/')

    context = {'form':obj, 'form1':obj1, 'title':f'Update employee:{qs}'}
    template_name= 'employees/form.html'
    return render(request, template_name, context)
