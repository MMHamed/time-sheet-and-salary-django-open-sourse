from django.db import models
from django.conf import settings #for use the AUTH.user_MODEL
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver





class Department(models.Model):
    Dep_name = models.CharField(max_length=200)

    def __str__(self):   # this to make if you query on that class, you will that object only not all the class
        return self.Dep_name

Salary_base_CHOISES =(    #to make salary_base choice without foreignkey in models
        ('Monthly','Monthly-base'),
        ('Hourly','Hourly-base')
    )
TimeSheet_base=(
        ('daily','Daily-group-base'),
        ('monthly','Monthly-self-base'),
        ('FingerPrint','Access-base')
    )

#extend the user create in the administrator date to make him an employee too
class Profile(models.Model): #this use to extend the user created by authen data with extra information
    Full_name = models.OneToOneField(User, on_delete=models.CASCADE)
    National_id = models.DecimalField(max_digits=30, decimal_places=0, unique=True, null=True)
    Address = models.CharField(max_length=200, null=True)
    Department = models.ForeignKey(Department,null=True, on_delete=models.SET_NULL)
    Emp_date = models.DateTimeField('employment date',default = timezone.now ,auto_now=False, auto_now_add=False)
    Salary_base = models.CharField(max_length=200, choices=Salary_base_CHOISES, default='Monthly')
    Salary = models.DecimalField(max_digits=250, decimal_places=2,null=True)
    Vacation_per_year = models.DecimalField(max_digits=250, decimal_places=2,default=21)
    TimeSheet_base = models.CharField(max_length=250, default= 'daily', choices = TimeSheet_base)
    updated = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.Full_name.username

@receiver(post_save, sender=User) #this to create the extended profile when the new user is created
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(Full_name=instance)

@receiver(post_save, sender=User) #this to save the extended profile when the user is updated
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

class Profile_document(models.Model):
    #Emp_name = models.ForeignKey(Profile, on_delete=models.CASCADE) #ForeignKey for one to many relationship
    Emp_name = models.OneToOneField(Profile, on_delete=models.CASCADE) #OneToOneField for one to one relationship = ForeignKey + unique=true // on delete= delete this related record 
    Photo = models.ImageField(upload_to='image/',blank=True, null=True)  
    
    def __str__(self):
        return self.Emp_name


#employee without user name and password (field, sales ...)
class Employee(models.Model):
    # Salary_base_CHOISES =(    #to make salary_base choice without foreignkey in models
    #     ('Monthly','Monthly-base'),
    #     ('Hourly','Hourly-base')
    # )
    # TimeSheet_base=(
    #     ('daily','daily-base'),
    #     ('monthly','monthly-base')
    # )
    #employee_id = models.AutoField(primary_key=True)
    user  = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, null=True, on_delete=models.SET_NULL) #on delete the user set that field to NULL
    First_name = models.CharField(max_length=200, null=True, blank=True)
    Last_name = models.CharField(max_length=200, null=True, blank=True)
    Full_name = models.CharField(max_length=200, unique=True)
    National_id = models.DecimalField(max_digits=30, decimal_places=0, unique=True)
    Address = models.CharField(max_length=200)
    Department = models.ForeignKey(Department,null=True, on_delete=models.SET_NULL)
    Emp_date = models.DateTimeField('employment date',default = timezone.now ,auto_now=False, auto_now_add=False)
    Salary_base = models.CharField(max_length=200, choices=Salary_base_CHOISES, default='Monthly')
    Salary = models.DecimalField(max_digits=250, decimal_places=2,null=True)
    Vacation_per_year = models.DecimalField(max_digits=250, decimal_places=2,default=21)
    TimeSheet_base = models.CharField(max_length=250, default= 'daily', choices = TimeSheet_base)
    updated = models.DateTimeField(auto_now=True)
    
    
    def __str__(self):
        return self.Full_name


class Employee_document(models.Model):
    #Emp_name = models.ForeignKey(Employee, on_delete=models.CASCADE) #ForeignKey for one to many relationship
    Emp_name = models.OneToOneField(Employee, on_delete=models.CASCADE) #OneToOneField for one to one relationship = ForeignKey + unique=true // on delete= delete this related record 
    Photo = models.ImageField(upload_to='image/',blank=True, null=True)  
    
    def __str__(self):
        return self.Emp_name.Full_name

# class combine_employee(Employee):
#     com_employee = models.CharField(max_length=75, blank=True, null=True)
#     class Meta:
#         db_table = 'combine_tbl'
#         managed = False


# class combine_profile(Profile):
#     com_profile = models.CharField(max_length=75, blank=True, null=True)
#     class Meta:
#         db_table = 'combine_tbl'
#         managed = False
        
    
