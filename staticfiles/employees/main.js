// C:\Users\nada-\Downloads\Mohamed\WEB_Developper\Python\Django\TS_and_salary\salary_and_ts\staticfiles\employees\main.js

//<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>


/********for show more/less in employee detail*********/
$(document).ready(function(){
    $(".nav-toggle").click(function(){
        //console.log("arrived");
        var collapse_content_selector = $(this).attr('href');
        var toggle_switch = $(this);
        $(collapse_content_selector).toggle(function(){
            if ($(this).css('display') == 'none') {
                toggle_switch.html('Read More');
            } else {
                toggle_switch.html('Read Less');
            }
        });
    });
}); /******end of show more/less in employee detail******* */

/************for the dependent dropdown select with django-select2 in search  */
$(document).ready(function(){
    $("#id_departement").change(function(){
        //console.log('arrived');
        var url = $("#empByDep").attr("data-emp-url");
        var depaID = $(this).val();
        //console.log(depaID);
        //console.log(url);

        $.ajax({
            //type:'GET',
            url: url,
            data:{
                'depaID' : depaID
            },
            //dataType : 'json',
            //async: true,
            success: function(data){
                //console.log(data);
                $('#id_employee_list').html(data);
            }
        });
    });
});  /*******end of dependent dropdown select with django-select2 in search****** */


/******Save/submit/approve get the group timesheet table contain and send it to django by GET for Save*******/
$(document).ready(function(){
    $('.SaSuApTblAllEmp').click(function(){
        var bt_id = $(this).attr('id');
        //alert (bt_id);
        if (bt_id == 'SaveTblAllEmp'){
            var RowNum = $('.table tbody tr').length;  //count the row of the table
            var ColNum = $('.table tbody tr td').length; //count the td of the table (number of cell in the table independant of row)
            //var ColNum = ColNum/RowNum;  //number of column in the table
            //var cell = $('.table tbody tr').find('td').eq(4).children().val();  // to get the  contain of a given cell in a table
            var TsDate = $('#TS_date').val();  //work
            var status = 'save'
            // var cell = $('.Working_Hour').val();  //work
            // alert(cell);
            var _res= new Array();
            var y = 1;
            var x = 0;
            for (y=1; y<ColNum; y=y+5) {
                _res[x] = [$('.table tbody tr').find('td').eq(y).html() , $('.table tbody tr').find('td').eq(y+3).children().val() ];
                x++  
            } ;
            
        } // end of if (bt_id == SaveTblAllEmp)

        if (bt_id == 'SubAppTblAllEmp'){
            var TsDate = $('#TS_date').val();
            var RowNum = $('.table tbody tr').length;
            var ColNum = $('.table tbody tr td').length; 
            var status = 'Submit_approve'
            var _res= new Array();
            var y = 1;
            var x = 0;
            for (y=1; y<ColNum; y=y+5) {
                _res[x] = [$('.table tbody tr').find('td').eq(y).html() , $('.table tbody tr').find('td').eq(y+3).children().val() ];
                if (_res[x][1]== '') {
                    console.log(_res[x][1]);
                    alert ('You did not select the working hour for all employees');
                    return false;
                }
                x++  
            } ;
        }//end of if (bt_id == SubAppTblAllEmp)

            var el = document.getElementsByName("csrfmiddlewaretoken"); //to get the token from HTML
            csrf_value = el[0].getAttribute("value"); //put the token in a variable
                //alert(TsDate +' , '+ RowNum + ' , '+ status )
            $.ajax({
                
                type:'POST',
                url: '/ajax/save_ts/',
                data:{
                    'res': _res ,
                    'TsDate': TsDate,
                    'RowNum': RowNum,
                    'status': status,
                    csrfmiddlewaretoken: csrf_value,  //dend the token for type post to the view
                },
                //dataType : 'json',  //make problem with back data
                async: true,
                success: function(data){
                    console.log(data);
                    alert (data);
                    
                    window.location.reload(true)
                },
                failure:function(data){
                    console.log(data);
                    alert('Have error');
                }
            }); 
            return false
    });
})/******end of SAVE/submit/approve group timesheet table contain All_employee and send it to django for save */


/******get the group timesheet if it is found in the dataBase  */
$(document).ready(function(){
    $('#TS_date').change(function(){
        var el = document.getElementsByName("csrfmiddlewaretoken"); //to get the token from HTML
        csrf_value = el[0].getAttribute("value"); //put the token in a variable
        for (x=0; x<RowNum ;x++){
            var y = document.getElementsByClassName('Working_Hour')[x];
            y.disabled = false;  //to enable
        }
        $('#SaveTblAllEmp').attr('disabled',false);  //to enable the button
        $('#SubAppTblAllEmp').attr('disabled',false);
        var Tsdate = $('#TS_date').val();
        var RowNum = $('.table tbody tr').length;
      $.ajax({
            
        type:'POST',
        url: '/GrpTimesheet/',
        data:{
            'TsDate': Tsdate,
            csrfmiddlewaretoken: csrf_value,  //dend the token for type post to the view
        },
        dataType : 'json',  //make problem with back data
        async: true,
        success: function(data){
            console.log(data);
            //alert (data.message);
            if (data.message) { //was created before
                
                if(data.weekend){
                    $('#TS_stat').html('This is a Weekend');
                }else if(data.NaVac){
                    $('#TS_stat').html('This is a National Vacation');
                }
                else {$('#TS_stat').html(data.message)};
                var z = 1
                for (x=0; x<RowNum ;x++){
                    var y = document.getElementsByClassName('Working_Hour')[x];
                    
                    RowEmpID = parseInt($('.table tbody tr').find('td').eq(z).html()) //to convert it from string to integer if JS 
                    if ( data.apVacEmp.indexOf(RowEmpID) >= 0 && data.apExVacEmp.indexOf(RowEmpID) >= 0 ){
                        y.value = 0;  //vacation out of limit
                        y.disabled =true;
                       EmpName =  $('.table tbody tr').find('td').eq(z+1).html()
                       $('.table tbody tr').find('td').eq(z+1).html(EmpName +'(extra vacation)')
                    }
                    // vacation in the limit
                    else if (data.apVacEmp.indexOf(RowEmpID) >= 0 && data.apExVacEmp.indexOf(RowEmpID) < 0 ){
                        y.value = 8;
                        y.disabled =true;
                        EmpName =  $('.table tbody tr').find('td').eq(z+1).html()
                       $('.table tbody tr').find('td').eq(z+1).html(EmpName +'(vacation)')
                    }
                    else {
                        y.disabled = false;  //to enable
                    }
                    z= z+5;
                }

                $('#SaveTblAllEmp').attr('disabled',false);  //to enable the button
                $('#SubAppTblAllEmp').attr('disabled',false);
            }
            if (data.message == "Submit/Approve"){  //was submited/approve before
                for (x=0; x<RowNum ;x++){
                    var y = document.getElementsByClassName('Working_Hour')[x];
                    y.disabled = true;  //to disable
                };
                $('#SaveTblAllEmp').attr('disabled', true) ; //to disable the button
                $('#SubAppTblAllEmp').attr('disabled', true) ;
            };
            if (data.qs){ //if it was save or  submit/approve yet
               r=$.parseJSON(data.qs);
               
               //window.location = 'http://127.0.0.1:8000/timesheet/?sort=id'; // after the that any other command is ignore (return false)
               var z = 1
               for (x=0; x<RowNum ;x++){
                    var y = document.getElementsByClassName('Working_Hour')[x];
                    //extra vacation
                    RowEmpID = parseInt($('.table tbody tr').find('td').eq(z).html()) //to convert it from string to integer if JS 
                    if ( data.apVacEmp.indexOf(RowEmpID) >= 0 && data.apExVacEmp.indexOf(RowEmpID) >= 0 ){
                        y.value = 0;  //vacation out of limit
                        y.disabled =true;
                       EmpName =  $('.table tbody tr').find('td').eq(z+1).html()
                       $('.table tbody tr').find('td').eq(z+1).html(EmpName +'(extra vacation)')
                    }
                    // vacation in the limit
                    else if (data.apVacEmp.indexOf(RowEmpID) >= 0 && data.apExVacEmp.indexOf(RowEmpID) < 0 ){
                        y.value = 8;
                        y.disabled =true;
                        EmpName =  $('.table tbody tr').find('td').eq(z+1).html()
                       $('.table tbody tr').find('td').eq(z+1).html(EmpName +'(vacation)')
                    }
                    else{y.value = r[x].fields.working_hour;};  //work at last
                    z= z+5;
                }
            }
            
            if(data.message1){ //not created yet
                if(data.weekend){
                    $('#TS_stat').html(data.weekend);
                    $('#TS_stat').css({"color":"red", "font-style": "oblique"});
                }else if(data.NaVac){
                    $('#TS_stat').html(data.NaVac);
                    $('#TS_stat').css({"color":"red", "font-style": "oblique"});
                }
                else {
                    $('#TS_stat').html(data.message1)
                var z = 1   //the column number 
                for (var x=0; x<RowNum; x++){
                    var y = document.getElementsByClassName('Working_Hour')[x];
                    //alert(data.apVacEmp.indexOf(parseInt($('.table tbody tr').find('td').eq(z).html())) +' '+ data.apVacEmp +' '+ $('.table tbody tr').find('td').eq(z).html());
                    RowEmpID = parseInt($('.table tbody tr').find('td').eq(z).html()) //to convert it from string to integer if JS 
                    if ( data.apVacEmp.indexOf(RowEmpID) >= 0 && data.apExVacEmp.indexOf(RowEmpID) >= 0 ){
                        y.value = 0;  //vacation out of limit
                        y.disabled =true;
                       EmpName =  $('.table tbody tr').find('td').eq(z+1).html()
                       $('.table tbody tr').find('td').eq(z+1).html(EmpName +'(extra vacation)')
                    }
                    // vacation in the limit
                    else if (data.apVacEmp.indexOf(RowEmpID) >= 0 && data.apExVacEmp.indexOf(RowEmpID) < 0 ){
                        y.value = 8;
                        y.disabled =true;
                        EmpName =  $('.table tbody tr').find('td').eq(z+1).html()
                       $('.table tbody tr').find('td').eq(z+1).html(EmpName +'(vacation)')
                    }
                    else {
                        y.value = ""; //empty value
                        y.disabled = false;  //to enable
                        
                    }
                    z= z+5;
                }
                
                $('#SaveTblAllEmp').attr('disabled',false); //to enable the button
                $('#SubAppTblAllEmp').attr('disabled', false);
            }
            }
            /*alert('QuerySet_first obj = '+ r[0]);
            document.write ('QuerySet_first obj = '+ r[0] +'<br><br>');
            var x;
            document.write ('QuerySet_first obj_detail'+ '<br>')

            for (x in r[0]){
                document.write ( x + ' = ' +r[0][x] +"<br>" );
            }
            document.write ('<br>'+'QuerySet_first obj_subObject_detail'+ '<br>')    
            for (x in r[0].fields){
                document.write (x + ' = ' + r[0].fields[x] + '<br>');
            } */
            
           },
        failure:function(data){
            console.log(data);
            alert('Have error');
        }
    }); 
      return false
    });

})/**end of get the group timesheet if it is found in the dataBase */

/********* Save/submit single timesheet in database ********** */
 $(document).ready(function(){
    $(".SaSuTblEmp").click(function(){
        var btID = $(this).attr('id');
        var TSYear = $('#TSYear').html();
        var TSMonth = $('#TSMonth').html();
        var emp_id = $('#emp_name').html();
        var RowNum = $('.table tbody tr').length;
        var ColNum = $('.table tbody tr td').length; //the  number of td in the holl table
        //alert(emp_id+','+TSYear+','+TSMonth );
        //console.log($('.table tbody tr').find('td').eq(0).html())
        var _res= new Array();
        var y = 0;
        var x = 0;
        for (y=0; y<ColNum; y=y+5) {
            _res[x] = [$('.table tbody tr').find('td').eq(y).html() , $('.table tbody tr').find('td').eq(y+2).children().val() ];
            //console.log(_res[x])
            x++
           
        }  
        
        if (btID == 'SaveTblEmp'){
            var status = 'saveSgTS'
        }
        
        else if(btID =='SubTblEmp'){
            var status = 'submitSgTS'
            
            for (var y=0; y<RowNum; y++) {
                console.log(y +', '+_res[y][1]);
                if (_res[y][1] == '' ) {
                    alert ('You did not select the working hour for all days');
                    return false;
                }
            }
        }
        var el = document.getElementsByName("csrfmiddlewaretoken"); //to get the token from HTML
        csrf_value = el[0].getAttribute("value"); //put the token in a variable
            //alert(TsDate +' , '+ RowNum + ' , '+ status )
        $.ajax({
            
            type:'POST',
            url: '/ajax/save_ts/',
            data:{
                'res': _res ,
                'emp_id': emp_id,
                'TSMonth': TSMonth,
                'TSYear': TSYear,
                'RowNum': RowNum,
                'status': status,
                csrfmiddlewaretoken: csrf_value,  //dend the token for type post to the view
            },
            //dataType : 'json',  //make problem with back data
            async: true,
            success: function(data){
                console.log(data);
                alert (data);
                window.location.replace("/SglTimesheet/");
            },
            failure:function(data){
                console.log(data);
                alert('Have error');
            }
        }); 
        return false
    })
})/*********end of Save/submit single timesheet in database ********** */

/***********approve/reject the timeSheet******* */
$(document).ready(function(){
    $(".ApTblEmp").click(function(){
        btID = $(this).attr('id');
        empID= $('#empID').html();
        console.log(empID)
        var el = document.getElementsByName("csrfmiddlewaretoken"); //to get the token from HTML
        csrf_value = el[0].getAttribute("value"); //put the token in a variable
            //alert(TsDate +' , '+ RowNum + ' , '+ status )
        $.ajax({
            
            type:'POST',
            url:'',     // empID + '/apporveTS/', //no need for url as we are in the same view!
            data:{
                'status':btID, 
                csrfmiddlewaretoken: csrf_value,  //dend the token for type post to the view
            },
            //dataType : 'json',  //make problem with back data
            async: true,
            success: function(data){
                console.log(data);
                alert (data.message);
                location.reload(true)
            },
            failure:function(data){
                console.log(data);
                alert('Have error');
            }
        }); 
        return false
    })
})/***********end of approve/reject the timeSheet******* */

/*************update the vacation number of days and the remaning number of days */
$(document).ready(function(){
    $("#id_End_date").change(function(){
        var remDay = $('#remDay').html();
        var endDate = new Date( $("#id_End_date").val() );
        var startDate = new Date( $('#id_start_date').val() );
        var vacNumDay = ((endDate - startDate)/(24*60*60*1000))+1;
        var AfReqDay1 = remDay - vacNumDay;
        $('#AfReqDay').html(AfReqDay1);
        $('#vacDay').html(vacNumDay);
        
    })
}) /*************end of update the vacation number of days and the remaning number of days */

/*********Get the remaining vacation number of day from dataBase*******/
$(document).ready(function(){
    $('.vac_emp_name').change(function(){
        var emp_id = $(this).val();
        var el = document.getElementsByName("csrfmiddlewaretoken"); //to get the token from HTML
        csrf_value = el[0].getAttribute("value"); //put the token in a variable
        $.ajax({
            
            type:'POST',
            url:'/vacationForm/',
            data:{
                'emp_id':emp_id, 
                csrfmiddlewaretoken: csrf_value,  //dend the token for type post to the view
            },
            //dataType : 'json',  //make problem with back data
            async: true,
            success: function(data){
                console.log(data);
                $('#remDay').html(data);
            },
            failure:function(data){
                console.log(data);
                alert('Have error');
            }
        }); 
        return false
        

    })
})/*********Get the remaining vacation number of day from dataBase*******/

/***********approve/reject the vacation ******* */
$(document).ready(function(){
    $(".ApTblVac").click(function(){
        btID = $(this).attr('id'); //to get if the approve or reject btn is pressed
        empID= $('#empID').html();
        //console.log(empID)
        var el = document.getElementsByName("csrfmiddlewaretoken"); //to get the token from HTML
        csrf_value = el[0].getAttribute("value"); //put the token in a variable
            
        $.ajax({
            
            type:'POST',
            url:'',     // empID + '/apporveTS/', //no need for url as we are in the same view!
            data:{
                'status':btID, 
                csrfmiddlewaretoken: csrf_value,  //dend the token for type post to the view
            },
            //dataType : 'json',  //make problem with back data
            async: true,
            success: function(data){
                console.log(data);
                alert (data);
                $('#TS_stat').html(data);
            },
            failure:function(data){
                console.log(data);
                alert('Have error');
            }
        }); 
        return false
    })
})/***********end of approve/reject the vacation******* */

/********* Save/submit/approve annual increase in database ********** */
$(document).ready(function(){
    $(".SaSuApTblAnInc").click(function(){
        var btID = $(this).attr('id');
        var AIDate = $('#AIDate').val();
        var SheetStat = $('#TS_stat').html();
        var RowNum = $('.table tbody tr').length;
        var ColNum = $('.table tbody tr td').length; //the  number of td in the holl table
        //alert(btID+','+ AIDate );
        //console.log($('.table tbody tr').find('td').eq(0).html())
        var _res= new Array();
        var y = 0;
        var x = 0;
        if (SheetStat == 'Not Created'){
            for (y=0; y<ColNum; y=y+8) {
                _res[x] = [$('.table tbody tr').find('td').eq(y+1).html() , $('.table tbody tr').find('td').eq(y+5).children().children('input').val(), $('.table tbody tr').find('td').eq(y+6).children().children('input').val(), $('.table tbody tr').find('td').eq(y+7).html() ];
                x++
            }
        } else{
            for (y=0; y<ColNum; y=y+9) {
                _res[x] = [$('.table tbody tr').find('td').eq(y+1).html() , $('.table tbody tr').find('td').eq(y+5).children().children('input').val(), $('.table tbody tr').find('td').eq(y+6).children().children('input').val(), $('.table tbody tr').find('td').eq(y+8).html() ];
                x++
            }
        } 
        
        if (btID == 'SaveTblAnInc'){
            var status = 'Save'
        }
        
        else if(btID =='AppTblAnInc'){
            var status = 'Appove'
            
            for (var y=0; y<RowNum; y++) {
                //console.log(y +', '+_res[y][1]);
                if ( _res[y][1] == '' || _res[y][1] == 'None' || _res[y][2] == ''|| _res[y][2] == 'None' ) {
                    alert ('You did not add all the field in the sheet');
                    return false;
                }
            }
        }
        var el = document.getElementsByName("csrfmiddlewaretoken"); //to get the token from HTML
        csrf_value = el[0].getAttribute("value"); //put the token in a variable
            //alert(TsDate +' , '+ RowNum + ' , '+ status )
            console.log(_res)
            
        
        $.ajax({
            
            type:'POST',
            url: '/SaveAnInc/',
            data:{
                'res': _res ,
                'AIDate': AIDate,
                'RowNum': RowNum,
                'status': status,
                csrfmiddlewaretoken: csrf_value,  //dend the token for type post to the view
            },
            //dataType : 'json',  //make problem with back data
            async: true,
            success: function(data){
                console.log(data);
                // window.location.replace("/AnInc/");
                alert (data);
                location.reload();

            },
            failure:function(data){
                console.log(data);
                alert('Have error');
            }
        }); 
        return false
    })
})/*********end of Save/submit/approve annual increase in database ********** */