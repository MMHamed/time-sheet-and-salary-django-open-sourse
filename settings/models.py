from django.db import models
from django.conf import settings

class NationalVacation(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    NationalVacation = models.CharField(max_length=250)
    Date = models.DateField(auto_now_add = False)
    numDay = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.NationalVacation

class WorkingDay(models.Model):
    
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    days = models.CharField(max_length=250, default= None) #add the default to remove the ---- from choise 
    workDay =models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.days

class OverTime(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    OTCat = models.CharField(max_length=250, default= None) #add the default to remove the ---- from choise 
    CatRate = models.FloatField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.OTCat

class TravelAllowance(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    TACat = models.CharField(max_length=250, default= None) #add the default to remove the ---- from choise 
    CatRate = models.FloatField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.TACat

class TaxRate(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    TaxCat = models.CharField(max_length=250, default= None) #add the default to remove the ---- from choise 
    TaxStart = models.FloatField(blank=True, null=True)
    TaxEnd = models.FloatField(blank=True, null=True)
    TaxRate = models.FloatField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.TaxCat

class SocialInsurance(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    SICat = models.CharField(max_length=250, default= None) #add the default to remove the ---- from choise 
    Amount = models.FloatField(blank=True, null=True)
    Company_Rate = models.FloatField(blank=True, null=True)
    Employee_Rate = models.FloatField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.SICat

class HealthInsurance(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    HICat = models.CharField(max_length=250, default= None) #add the default to remove the ---- from choise 
    HIRate = models.FloatField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.HICat

class Shifts(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    ShiftName = models.CharField(max_length=250, default= None)
    ShiftStart = models.TimeField(blank=True, null=True) #add the default to remove the ---- from choise 
    ShiftEnd = models.TimeField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.ShiftName

class SingleMultiUser(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    Single_Multi_name = models.CharField(max_length=250, default= None) #add the default to remove the ---- from choise 
    Single_Multi_choice = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.Single_Multi_name