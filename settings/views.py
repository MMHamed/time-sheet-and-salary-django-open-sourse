from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.forms import modelformset_factory
from django_tables2.config import RequestConfig
from .models import NationalVacation, WorkingDay, OverTime,TravelAllowance, TaxRate, SocialInsurance, HealthInsurance, Shifts, SingleMultiUser
from .forms import NaVacationForm, WorkingDayForm, OverTimeForm,TravelAllowanceForm, TaxRateForm, SocialInsuranceForm, HealthInsuranceForm, ShiftsForm, SingleMultiUserForm, DepartementForm
from .tables import NVacListTBL, WorkDayTbl, OverTimeTbl,TravelAllowanceTbl, TaxRateTbl, SocialInsuranceTbl, HealthInsuranceTbl, ShiftsTbl, SingleMultiTbl, DepartmentTbl
from employees.models import Department

@login_required
def NaVaction_view(request):
    
    NaVacationFormSet= modelformset_factory(
    NationalVacation, 
    form= NaVacationForm ,
    extra=1
    )
    if request.method == 'POST':
        formset = NaVacationFormSet(request.POST, None)
        if formset.is_valid():
            instances = formset.save()  # this is very good if you don't want to add the user
        #print(formset)
        # count = len(formset) #not fully work.this to save the user without Date constrain not null for the extra=1 problem!! 
        # print(count)
        # x=1
        # for form in formset:
            
        #     if form['NationalVacation'].value()!=None or form['Date'].value() != None:
        #         print(form['NationalVacation'].value(), form['Date'].value() )
        #         f= form.save(commit=False)
        #         f.user = request.user
        #         f.save()
                  
                  
    formset = NaVacationFormSet()
    table = NVacListTBL(formset)
    RequestConfig(request, paginate={"per_page": 25}).configure(table)

    template ='settings/setview.html'
    context={'form':formset, 'table':table, 'title': 'National Vacation'}
    return render(request, template, context)

@login_required
def Workingday_view(request):
    WoprkingDayFormSet= modelformset_factory(
    WorkingDay, 
    form= WorkingDayForm,
    extra=0 
    )
    if request.method == 'POST':
        form = WoprkingDayFormSet(request.POST, None)
        if form.is_valid(): 
            instances = form.save()
                
    form = WoprkingDayFormSet()  
    # table = WorkDayTbl(form)
    # RequestConfig(request, paginate={"per_page": 25}).configure(table)
    template ='settings/setview.html'
    context = {'form':form, 'title': 'Working Days', 'Workingday_view':True}
    return render(request, template, context)

@login_required
def SingleMultiUser_view(request):
    SingleMultiFormSet= modelformset_factory(
    SingleMultiUser, 
    form= SingleMultiUserForm,
    extra=1 
    )
    if request.method == 'POST':
        form = SingleMultiFormSet(request.POST, None)
        if form.is_valid():
            instances = form.save(commit=False)
            form.user= request.user
            form.save()   

    form = SingleMultiFormSet()  
    # table = SingleMultiTbl(form)
    # RequestConfig(request, paginate={"per_page": 25}).configure(table)
    template ='settings/setview.html'
    context = {'form':form, 'title': 'Single or Multi User system','SingleMultiUser_view':True, 'note':'*If all are checked or unchecked it will be consider single . 1=single/  2=multi'}
    return render(request, template, context)

@login_required
def OverTime_view(request):
    OverTimeFormSet= modelformset_factory(
    OverTime, 
    form= OverTimeForm,
    extra=1 
    )
    if request.method == 'POST':
        form = OverTimeFormSet(request.POST, None)
        if form.is_valid(): 
            instances = form.save()
                
    form = OverTimeFormSet()  
    table = OverTimeTbl(form)
    RequestConfig(request, paginate={"per_page": 25}).configure(table)
    template ='settings/setview.html'
    context = {'form':form,'table':table, 'title': 'Over Time'}
    return render(request, template, context)

@login_required
def TravelAllowance_view(request):
    TravelAllowanceFormSet= modelformset_factory(
    TravelAllowance, 
    form= TravelAllowanceForm,
    extra=1 
    )
    if request.method == 'POST':
        form = TravelAllowanceFormSet(request.POST, None)
        if form.is_valid(): 
            instances = form.save()
                
    form = TravelAllowanceFormSet()  
    table = TravelAllowanceTbl(form)
    RequestConfig(request, paginate={"per_page": 25}).configure(table)
    template ='settings/setview.html'
    context = {'form':form,'table':table, 'title': 'Travel Allowance'}
    return render(request, template, context)

@login_required
def TaxRate_view(request):
    TaxRateFormSet= modelformset_factory(
    TaxRate, 
    form= TaxRateForm,
    extra=1 
    )
    if request.method == 'POST':
        form = TaxRateFormSet(request.POST, None) 
        if form.is_valid():
            instances = form.save()
        #print(form)            
    form = TaxRateFormSet()  
    table = TaxRateTbl(form)
    RequestConfig(request, paginate={"per_page": 25}).configure(table)
    template ='settings/setview.html'
    context = {'form':form,'table':table, 'title': 'Tax Rate'}
    return render(request, template, context)

@login_required
def SocialInsurance_view(request):
    SocialInsuranceFormSet= modelformset_factory(
    SocialInsurance, 
    form= SocialInsuranceForm,
    extra=1 
    )
    if request.method == 'POST':
        form = SocialInsuranceFormSet(request.POST, None)
        if form.is_valid(): 
            instances = form.save()
                
    form = SocialInsuranceFormSet()  
    table = SocialInsuranceTbl(form)
    RequestConfig(request, paginate={"per_page": 25}).configure(table)
    template ='settings/setview.html'
    context = {'form':form,'table':table, 'title': 'Social Insurance'}
    return render(request, template, context)

@login_required
def HealthInsurance_view(request):
    HealthInsuranceFormSet= modelformset_factory(
    HealthInsurance, 
    form= HealthInsuranceForm,
    extra=1 
    )
    if request.method == 'POST':
        form = HealthInsuranceFormSet(request.POST, None)
        if form.is_valid(): 
            instances = form.save()
                
    form = HealthInsuranceFormSet()  
    table = HealthInsuranceTbl(form)
    RequestConfig(request, paginate={"per_page": 25}).configure(table)
    template ='settings/setview.html'
    context = {'form':form,'table':table, 'title': 'Health Insurance'}
    return render(request, template, context)

@login_required
def Shifts_view(request):
    ShiftsFormSet= modelformset_factory(
    Shifts, 
    form= ShiftsForm,
    extra=1 
    )
    if request.method == 'POST':
        form =ShiftsFormSet(request.POST, None)
        if form.is_valid(): 
            instances = form.save()
                
    form = ShiftsFormSet()  
    table =ShiftsTbl(form)
    RequestConfig(request, paginate={"per_page": 25}).configure(table)
    template ='settings/setview.html'
    context = {'form':form,'table':table, 'title': 'Shifts Time'}
    return render(request, template, context)

@login_required
def Department_view(request):
    DepartementFormSet= modelformset_factory(
    Department, 
    form= DepartementForm,
    extra=1 
    )
    if request.method == 'POST':
        form =DepartementFormSet(request.POST, None)
        if form.is_valid(): 
            instances = form.save()
                
    form = DepartementFormSet()  
    table =DepartmentTbl(form)
    RequestConfig(request, paginate={"per_page": 25}).configure(table)
    template ='settings/setview.html'
    context = {'form':form,'table':table, 'title': 'Departments'}
    return render(request, template, context)