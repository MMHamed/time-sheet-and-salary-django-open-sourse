from django.contrib import admin
from .models import NationalVacation, WorkingDay, OverTime,TravelAllowance, TaxRate, SocialInsurance, HealthInsurance, Shifts, SingleMultiUser


admin.site.register(NationalVacation)
admin.site.register(WorkingDay)
admin.site.register(OverTime)
admin.site.register(TravelAllowance)
admin.site.register(TaxRate)
admin.site.register(SocialInsurance)
admin.site.register(HealthInsurance)
admin.site.register(Shifts)
admin.site.register(SingleMultiUser)