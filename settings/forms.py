from django.forms import modelformset_factory, DateInput, TextInput, NumberInput, CheckboxInput, NumberInput, TimeInput, Select, BaseFormSet, RadioSelect
from django_select2.forms import Select2Widget
from django import forms 
from .models import NationalVacation, WorkingDay, OverTime,TravelAllowance, TaxRate, SocialInsurance, HealthInsurance, Shifts, SingleMultiUser
from employees.models import Department

class NaVacationForm(forms.ModelForm):
    
    class Meta:
        model = NationalVacation
        fields = ['NationalVacation', 'Date','user']
        exclude = ()
        widgets={
            'Date' : DateInput(attrs={'class': 'form-control ', 'type':'date' },),
            'NationalVacation' :TextInput(attrs={'class': 'form-control ' },),
            'numDay' : NumberInput (attrs={'class': 'form-control ' },),
            
        }


class WorkingDayForm(forms.ModelForm):
    
    # days= forms.ModelMultipleChoiceField(
    #     queryset = WorkingDay.objects.all(),
    #     widget = forms.CheckboxSelectMultiple,
    #     required = False
    #)
    class Meta:
        model = WorkingDay
        fields = ['days', 'workDay','user']
        exlude = ()
        widgets = {
            'days' : TextInput(attrs={'class': 'form-control ' },),
            'workDay': CheckboxInput(attrs={'class': 'form-control ' ,'size':'10'},)
        }
    
    def __init__(self, *args, **kwargs):  # this to remove the main label field from the form'day'
        super(WorkingDayForm, self).__init__(*args, **kwargs)
        self.fields['days'].label = ''

class SingleMultiUserForm(forms.ModelForm):
    
    # days= forms.ModelMultipleChoiceField(
    #     queryset = WorkingDay.objects.all(),
    #     widget = forms.CheckboxSelectMultiple,
    #     required = False
    #)
    class Meta:
        model = SingleMultiUser
        fields = ['Single_Multi_name', 'Single_Multi_choice']#,'user']
        exlude = ()
        labels={
            'Single_Multi_name': 'System Kind',
            'Single_Multi_choice': 'Single/Multi-User'
        }
        widgets = {
            'Single_Multi_name' : TextInput(attrs={'class': 'form-control ' ,'size':'10' },),
            'Single_Multi_choice': CheckboxInput(attrs={'class': 'form-check-input' },)
        }
    
    def __init__(self, *args, **kwargs):  # this to remove the main label field from the form'day'
        super(SingleMultiUserForm, self).__init__(*args, **kwargs)
        self.fields['Single_Multi_name'].label = ''
        # self.fields['Single_Multi_choice'].label = ''


class OverTimeForm(forms.ModelForm):
    class Meta:
        model = OverTime
        fields = ['OTCat', 'CatRate','user']
        exlude = ()
        widgets = {
            'OTCat' : TextInput(attrs={'class': 'form-control ' },),
            'CatRate': NumberInput(attrs={'class': 'form-control ' },)
        }
    
    
class TravelAllowanceForm(forms.ModelForm):
    class Meta:
        model = TravelAllowance
        fields = ['TACat', 'CatRate','user']
        exlude = ()
        widgets = {
            'TACat' : TextInput(attrs={'class': 'form-control ' },),
            'CatRate': NumberInput(attrs={'class': 'form-control ' },)
        }
    
    
class TaxRateForm(forms.ModelForm):
    class Meta:
        model = TaxRate
        fields = ['TaxCat', 'TaxStart','TaxEnd','TaxRate','user']
        exlude = ()
        widgets = {
            'TaxCat' : TextInput(attrs={'class': 'form-control ' },),
            'TaxStart': NumberInput(attrs={'class': 'form-control ' },),
            'TaxEnd': NumberInput(attrs={'class': 'form-control ' },),
            'TaxRate': NumberInput(attrs={'class': 'form-control ' },)
        }
    
class SocialInsuranceForm(forms.ModelForm):
    class Meta:
        model = SocialInsurance
        fields = ['SICat', 'Amount','Company_Rate','Employee_Rate','user']
        exlude = ()
        widgets = {
            'SICat' : TextInput(attrs={'class': 'form-control ' },),
            'Amount': NumberInput(attrs={'class': 'form-control ' },),
            'Company_Rate': NumberInput(attrs={'class': 'form-control ' },),
            'Employee_Rate': NumberInput(attrs={'class': 'form-control ' },)
        }    

class HealthInsuranceForm(forms.ModelForm):
    class Meta:
        model = HealthInsurance
        fields = ['HICat', 'HIRate','user']
        exlude = ()
        widgets = {
            'HICat' : TextInput(attrs={'class': 'form-control ' },),
            'HIRate': NumberInput(attrs={'class': 'form-control ' },)
        }

class ShiftsForm(forms.ModelForm):
    class Meta:
        model = Shifts
        fields = ['ShiftName', 'ShiftStart','ShiftEnd','user']
        exlude = ()
        widgets = {
            'ShiftName' : TextInput(attrs={'class': 'form-control ' },),
            'ShiftStart': TimeInput(attrs={'class': 'form-control ','type':'time' },),
            'ShiftEnd': TimeInput(attrs={'class': 'form-control ', 'type':'time' },),
            
        }

class DepartementForm(forms.ModelForm):
    class Meta:
        model = Department
        fields = ['Dep_name']
        exlude = ()
        widgets = {
            'Dep_name' : TextInput(attrs={'class': 'form-control ' },),
                        
        }   