import django_tables2 as tables
from django import forms
import itertools
from django_tables2.utils import A
from django_tables2.columns import BoundColumns
from django.utils.safestring import mark_safe
from django.utils.html import format_html
from datetime import datetime

from .models import NationalVacation, WorkingDay, OverTime,TravelAllowance, TaxRate, SocialInsurance, HealthInsurance, Shifts, SingleMultiUser
from employees.models import Department

# class HiddenInputColumn(tables.Column): # not work -->to add  {{ my_formset.management_form }} to the table
#     def __init__(self, *args, **kwargs):
#         kwargs['attrs'] = {'th': {'style': 'display:none;'},
#                            'td': {'style': 'display:none;'}}
#         super(HiddenInputColumn, self).__init__(*args, **kwargs)

    # def render(self, value, record):
    #     return format_html('<input type="hidden" name="{}" value="{}" />',self.verbose_name, record[self.verbose_name])


class NVacListTBL(tables.Table):
    row_number = tables.Column(empty_values=())
    Day = tables.Column(empty_values=())
    Date = tables.Column()
    NationalVacation = tables.Column(verbose_name= 'National Vacation') 
    user = tables.Column(empty_values=(), attrs={'th':{'style':'display:none;'},
                                                 'td':{'style':'display:none;'}
                        })
    
    

    class Meta:
        model = NationalVacation
        fields= ['NationalVacation', 'Date']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'NationalVacation', 'Day', 'Date','user']

     
   
    
    def __init__(self, *args, **kwargs):
        super(NVacListTBL, self).__init__(*args, **kwargs)
        self.counter = itertools.count()  # should be here otherwise if we put "itertools.count()" the count will be reset 

        

    def render_row_number(self):
        def repeating_counter(times):
            return (x // times for x in self.counter)

        rc= repeating_counter(2)  # this how we can get from next the same number twice for row number and user (below)
        return '%d' % next(rc)

    def render_Day(self, value, record):
        Ddate= record['Date'].value()
        if Ddate != None:  #as the last empty row make problem with strptime (Ddate =None)
            Ddate = datetime.strptime(str(record['Date'].value()), '%Y-%m-%d')
            Ddate = Ddate.strftime("%A") 
            # print('*****************')
            # print(Ddate.strftime("%A"))
            weekEnd= WorkingDay.objects.filter(workDay=False)
            WE=[]
            for x in weekEnd:
                WE.append (x.days)
            # print('*****************')
            # print(WE)
            if Ddate  in WE: #== 'Friday' or Ddate == 'Saturday':  
                return format_html("<b style='color:red'>{}</b>",Ddate)
            else:
                return format_html("<b>{}</b>",Ddate) 
        #return "%s" %record['Date'].value()  #bounded field!!!!

    # def render_Date(self, value, record):
    #     Ddate= record['Date'].value()
    #     if Ddate != None:  #as the last empty row make problem (Ddate =None)
    #         Ddate = datetime.strptime(str(value.value()), '%Y-%m-%d')
    #         Ddate = Ddate.strftime("%A") 
    #         # print('*****************')
    #         # print(Ddate.strftime("%A"))
    #         if Ddate == 'Friday' or Ddate == 'Saturday':
    #             return "%s - %s" %(Ddate, value)
    #         else:
    #             return "%s - %s" %(Ddate, value) 
    # def before_render(self,request): #to hide the column
    #     self.columns.hide('user')

    def render_user(self, value, record):
        user2 = self.context["request"].user  #work to get the user from request
        x = self.render_row_number() #work call the function 
        
        
        if record['NationalVacation'].value() == None  or record['Date'].value() == None:
            return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}'>",
                                x,
                                x,
                                '',
                                ) 
        else:
             return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}' >",
                                x,
                                x,
                                user2.id,
                                ) 

    

     

    
class WorkDayTbl(tables.Table):
    row_number = tables.Column(empty_values=())
    days = tables.Column(verbose_name= 'Days of the week')
    workDay = tables.Column(verbose_name= 'Work Days')
    user = tables.Column(empty_values=(), attrs={'th':{'style':'display:none;'},
                                                 'td':{'style':'display:none;'}
                        })
    class Meta:
        model = WorkingDay
        fields= ['days', 'workDay']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'days', 'workDay','user']
  
    
    def __init__(self, *args, **kwargs):
        super(WorkDayTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        def repeating_counter(times):
            return (x // times for x in self.counter)

        rc= repeating_counter(2)  # this how we can get from next the same number twice for row number and user (below)
        return '%d' % next(rc)

    def render_user(self, value, record):
        user2 = self.context["request"].user  #work to get the user from request
        x = self.render_row_number() #work call the function 
        
        
        if record['days'].value() == None  or record['workDay'].value() == None:
            return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}'>",
                                x,
                                x,
                                '',
                                ) 
        else:
             return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}' >",
                                x,
                                x,
                                user2.id,
                                )         

##########checkbox not stable############
class SingleMultiTbl(tables.Table):
    row_number = tables.Column(empty_values=())
    Single_Multi_name = tables.Column(verbose_name= 'System Kind')
    Single_Multi_choice = tables.Column(verbose_name= 'Single or Multi')
    # Single_Multi_choice = tables.Column(empty_values=(), verbose_name= 'Single /Multi 1')
    user = tables.Column(empty_values=(), attrs={'th':{'style':'display:none;'},
                                                 'td':{'style':'display:none;'}
                        })
    class Meta:
        model = SingleMultiUser
        fields= ['Single_Multi_name', 'Single_Multi_choice']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'Single_Multi_name', 'Single_Multi_choice',  'user']
  
    
    def __init__(self, *args, **kwargs):
        super(SingleMultiTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        def repeating_counter(times):
            return (x // times for x in self.counter)

        rc= repeating_counter(2)  # this how we can get from next the same number twice for row number and user (below)
        return '%d' % next(rc)

    def render_user(self, value, record):
        user2 = self.context["request"].user  #work to get the user from request
        x = self.render_row_number() #work call the function 
        
        
        if record['Single_Multi_name'].value() == None  or record['Single_Multi_choice'].value() == None:
            return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}'>",
                                x,
                                x,
                                '',
                                ) 
        else:
             return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}' >",
                                x,
                                x,
                                user2.id,
                                )         
    
    #not work radio button
    # def render_Single_Multi_choice(self, value, record):
    #     x = self.render_row_number() #work call the function
    #     return format_html("<input id='id_form-{}-Single_Multi_choice' class='form-control' type='checkbox' name='form-{}-Single_Multi_choice' >", x, x )


class OverTimeTbl(tables.Table):
    row_number = tables.Column(empty_values=())
    OTCat = tables.Column(verbose_name= 'Over time category')
    CatRate = tables.Column(verbose_name= 'Category Rate')
    user = tables.Column(empty_values=(), attrs={'th':{'style':'display:none;'},
                                                 'td':{'style':'display:none;'}
                        })
    class Meta:
        model = OverTime
        fields= ['OTCat', 'CatRate']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'OTCat', 'CatRate','user']
  
    
    def __init__(self, *args, **kwargs):
        super(OverTimeTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        def repeating_counter(times):
            return (x // times for x in self.counter)

        rc= repeating_counter(2)  # this how we can get from next the same number twice for row number and user (below)
        return '%d' % next(rc)

    def render_user(self, value, record):
        user2 = self.context["request"].user  #work to get the user from request
        x = self.render_row_number() #work call the function 
        
        
        if record['OTCat'].value() == None  or record['CatRate'].value() == None:
            return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}'>",
                                x,
                                x,
                                '',
                                ) 
        else:
             return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}' >",
                                x,
                                x,
                                user2.id,
                                )         

class TravelAllowanceTbl(tables.Table):
    row_number = tables.Column(empty_values=())
    TACat = tables.Column(verbose_name= 'Travelling category')
    CatRate = tables.Column(verbose_name= 'Category Rate')
    user = tables.Column(empty_values=(), attrs={'th':{'style':'display:none;'},
                                                 'td':{'style':'display:none;'}
                        })
    class Meta:
        model = TravelAllowance
        fields= ['TACat', 'CatRate']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'TACat', 'CatRate','user']
  
    
    def __init__(self, *args, **kwargs):
        super(TravelAllowanceTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        def repeating_counter(times):
            return (x // times for x in self.counter)

        rc= repeating_counter(2)  # this how we can get from next the same number twice for row number and user (below)
        return '%d' % next(rc)

    def render_user(self, value, record):
        user2 = self.context["request"].user  #work to get the user from request
        x = self.render_row_number() #work call the function 
        
        
        if record['TACat'].value() == None  or record['CatRate'].value() == None:
            return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}'>",
                                x,
                                x,
                                '',
                                ) 
        else:
             return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}' >",
                                x,
                                x,
                                user2.id,
                                )         

class TaxRateTbl(tables.Table):
    row_number = tables.Column(empty_values=())
    TaxCat = tables.Column(verbose_name= 'Tax category')
    TaxStart = tables.Column(verbose_name= 'Category Start')
    TaxEnd = tables.Column(verbose_name= 'Category End')
    TaxRate = tables.Column(verbose_name= 'Category Rate')
    user = tables.Column(empty_values=(), attrs={'th':{'style':'display:none;'},
                                                 'td':{'style':'display:none;'}
                        })
    class Meta:
        model = TaxRate
        fields= ['TaxCat', 'TaxStart','TaxEnd','TaxRate']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'TaxCat', 'TaxStart','TaxEnd','TaxRate','user']
  
    
    def __init__(self, *args, **kwargs):
        super(TaxRateTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        def repeating_counter(times):
            return (x // times for x in self.counter)

        rc= repeating_counter(2)  # this how we can get from next the same number twice for row number and user (below)
        return '%d' % next(rc)

    def render_user(self, value, record):
        user2 = self.context["request"].user  #work to get the user from request
        x = self.render_row_number() #work call the function 
        
        
        if record['TaxCat'].value() == None  or record['TaxRate'].value() == None:
            return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}'>",
                                x,
                                x,
                                '',
                                ) 
        else:
             return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}' >",
                                x,
                                x,
                                user2.id,
                                )         
    
class SocialInsuranceTbl(tables.Table):
    row_number = tables.Column(empty_values=())
    SICat = tables.Column(verbose_name= 'Social Insurance category')
    Amount = tables.Column()
    Company_Rate = tables.Column()
    Employee_Rate = tables.Column()
    user = tables.Column(empty_values=(), attrs={'th':{'style':'display:none;'},
                                                 'td':{'style':'display:none;'}
                        })
    class Meta:
        model = SocialInsurance
        fields= ['SICat', 'Amount','Company_Rate','Employee_Rate']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'SICat', 'Amount','Company_Rate','Employee_Rate','user']
  
    
    def __init__(self, *args, **kwargs):
        super(SocialInsuranceTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        def repeating_counter(times):
            return (x // times for x in self.counter)

        rc= repeating_counter(2)  # this how we can get from next the same number twice for row number and user (below)
        return '%d' % next(rc)

    def render_user(self, value, record):
        user2 = self.context["request"].user  #work to get the user from request
        x = self.render_row_number() #work call the function 
        
        
        if record['SICat'].value() == None  or record['Amount'].value() == None:
            return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}'>",
                                x,
                                x,
                                '',
                                ) 
        else:
             return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}' >",
                                x,
                                x,
                                user2.id,
                                )         

class HealthInsuranceTbl(tables.Table):
    row_number = tables.Column(empty_values=())
    HICat = tables.Column(verbose_name= 'Health Insurance category')
    HIRate = tables.Column(verbose_name= 'Category Rate')
    user = tables.Column(empty_values=(), attrs={'th':{'style':'display:none;'},
                                                 'td':{'style':'display:none;'}
                        })
    class Meta:
        model = HealthInsurance
        fields= ['HICat', 'HIRate']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'HICat', 'HIRate','user']
  
    
    def __init__(self, *args, **kwargs):
        super(HealthInsuranceTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        def repeating_counter(times):
            return (x // times for x in self.counter)

        rc= repeating_counter(2)  # this how we can get from next the same number twice for row number and user (below)
        return '%d' % next(rc)

    def render_user(self, value, record):
        user2 = self.context["request"].user  #work to get the user from request
        x = self.render_row_number() #work call the function 
        
        
        if record['HICat'].value() == None  or record['HIRate'].value() == None:
            return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}'>",
                                x,
                                x,
                                '',
                                ) 
        else:
             return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}' >",
                                x,
                                x,
                                user2.id,
                                )         

class ShiftsTbl(tables.Table):
    row_number = tables.Column(empty_values=())
    ShiftName = tables.Column(verbose_name= 'Shift Name')
    ShiftStart = tables.Column(verbose_name= 'Shift Start time')
    ShiftEnd = tables.Column(verbose_name= 'Shift End time')
    user = tables.Column(empty_values=(), attrs={'th':{'style':'display:none;'},
                                                 'td':{'style':'display:none;'}
                        })
    class Meta:
        model = Shifts
        fields= ['ShiftName', 'ShiftStart','ShiftEnd']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'ShiftName', 'ShiftStart','ShiftEnd','user']
  
    
    def __init__(self, *args, **kwargs):
        super(ShiftsTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        def repeating_counter(times):
            return (x // times for x in self.counter)

        rc= repeating_counter(2)  # this how we can get from next the same number twice for row number and user (below)
        return '%d' % next(rc)

    def render_user(self, value, record):
        user2 = self.context["request"].user  #work to get the user from request
        x = self.render_row_number() #work call the function 
        
        
        if record['ShiftName'].value() == None  or record['ShiftEnd'].value() == None:
            return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}'>",
                                x,
                                x,
                                '',
                                ) 
        else:
             return  format_html("<input in='id_form-{}-user' name='form-{}-user' value='{}' >",
                                x,
                                x,
                                user2.id,
                                )         

class DepartmentTbl(tables.Table):
    row_number = tables.Column(empty_values=())
    Dep_name = tables.Column(verbose_name= 'Department name')
    
    class Meta:
        model = Department
        fields= ['Dep_name']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'Dep_name']
  
    
    def __init__(self, *args, **kwargs):
        super(DepartmentTbl, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        def repeating_counter(times):
            return (x // times for x in self.counter)

        rc= repeating_counter(1)  # this how we can get from next the same number twice for row number and user (below)
        return '%d' % next(rc)

    
