from django import forms
from django_select2.forms import Select2Widget

class UploadFPFile_form(forms.Form):
    formatchoise=[
        ('csv','csv'),
        ('xls','xls'),
        ('xlsx','xlsx'),
        ('tsv','tsv'),
        ('json','json'),
        ('yaml','yaml')
    ]
    FPFile = forms.FileField(
        label='Select Time sheet File',
        widget= forms.ClearableFileInput, 
        required=True,
        )
    fileformat=forms.ChoiceField(
        label='Select the file format',
        choices=formatchoise, 
        widget=Select2Widget(attrs={'data-placeholder':(u"Select"), 'class': 'form-control '})
        ,required=True
        )
    