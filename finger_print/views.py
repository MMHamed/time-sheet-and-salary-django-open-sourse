from django.shortcuts import render
from django.http import HttpResponse
import os
from datetime import time , date, timedelta, datetime
from tablib import Dataset
from .resources import AttFingerPrint
from .forms import UploadFPFile_form
import pandas as pd

def export_view(request): #excel format
    attendance_resource = AttFingerPrint()
    dataset= attendance_resource.export()
    response = HttpResponse(dataset.xls, content_type ='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="AttendanceFingerPrint.xls"'
    return response

def import_view(request):
    if request.method == 'POST':
        attendance_resource = AttFingerPrint()
        dataset= Dataset() #['' ,'08:01:40','01-11-2018','empX1','1'],headers=['id','Time','Date','NAME','ROLL_NO'])
        new_attend = request.FILES['FPFile']
        format = request.POST.get('fileformat', None)
        print ('uploaded file:', new_attend, format)
        #handle_uploaded_file(new_attend)  #work
        if format =='csv':
            imported_data = dataset.load(new_attend.read().decode('utf-8'), format= format)
        #print(imported_data)
        
        elif format =='xls':
            imported_data = dataset.load(new_attend.read(), format= format)
            y= 0
            xlsTime = []
            for x in dataset['Time']:
                x = int(x * 24 * 3600) # convert to number of seconds
                my_time = time(x//3600, (x%3600)//60, x%60) # hours, minutes, seconds
                xlsTime.append(my_time)
                dataset['Time'][y]= my_time
                #print(my_time, dataset['Time'][y], y )
                y +=1
            xlsDate = []
            for x in dataset['Date']:
                xlsDate.append(date(1899,12,31)+timedelta(x-1))
                
            del dataset['Time']
            del dataset['Date']
            dataset.insert_col(0, col=xlsTime, header="Time") #to add empty column for id
            dataset.insert_col(1, col=xlsDate, header="Date")
              
        else: #for xlsx  and the remaining format
            imported_data = dataset.load(new_attend.read(), format= format)
        
        #print(dataset['Time'])
        updated =[]
        id= []
        for x in dataset['Date']:
                updated.append(None)
                id.append(None)
                
        dataset.insert_col(5, col=updated, header="updated")
        dataset.insert_col(0, col=id, header="id")
        print(dataset)
        
        result = attendance_resource.import_data(dataset, dry_run =True)
        print(result.has_errors(), result)
        if not result.has_errors():
            print('no error found in the file')
            attendance_resource.import_data(dataset, dry_run =False)
    
    form = UploadFPFile_form()
    context= {'form':form, 'title':'Time Sheet upload'}
    template = 'finger_print/import_export.html'
    return render(request, template, context )



# def handle_uploaded_file(f):  #work
#     # with open('finger_print/desti.txt', 'w+') as destination:
#     x1= pd.ExcelFile(f)  
#     print(x1) #<pandas.io.excel._base.ExcelFile object at 0x0000024452656EB8>
#     df1= x1.parse('Sheet1') #dataframe used by panda
#     print(df1)
    
    #     destination.write(str(f.name))
    #     destination.write('\n')
    #     destination.write(str(f.size))
    #     destination.write('\n')
    #     destination.write(str(f.content_type))
    #     destination.write('\n')
    #     destination.write(str(f.content_type_extra))
    #     destination.write('\n')
    #     destination.write(str(f.charset))
    # with open('finger_print/desti.txt', 'ab+') as destination:
    #     for chunk in f.chunks():
    #         destination.write(chunk)
        # #destination.write('\n')
        # for line in f:
        #     destination.write(line)