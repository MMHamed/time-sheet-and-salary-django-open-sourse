from django.db import models
from employees.models import Profile, Employee
from django.conf import settings

class FingerPrint(models.Model):
    Time= models.TimeField()
    Date= models.DateField()
    NAME= models.CharField(max_length=200)
    ROLL_NO = models.ForeignKey(Profile, on_delete=models.CASCADE)
    #user  = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    updated = models.DateTimeField(auto_now=True)
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['Time','Date','NAME'],name='unique_timesheetFingerPrint')
            #each employee have only one finguer print in the day at the same time 
        ]
    def __str__(self):   # this to make if you query on that class, you will that object only not all the class
        return self.NAME