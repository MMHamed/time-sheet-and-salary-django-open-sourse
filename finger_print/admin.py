from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import FingerPrint


@admin.register(FingerPrint)
class FingerPrintAdmin(ImportExportModelAdmin):
    pass
