from django import forms
from django_select2.forms import Select2Widget
from django.forms import ModelChoiceField, widgets, SelectDateWidget
from employees.models import Employee , Employee_document, Department
from timesheets.models import TimeSheet
#from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField
from django.contrib.admin import widgets
from Salary.fun_SingleMultiUser import SingleMultiUser_fun


class NidModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.National_id

class AddModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.Address



class SearchFullName(forms.Form):
    
    departement = forms.ModelChoiceField(
        queryset=Department.objects.all(),
        #empty_label = u"departement",  #to change the default ----- with a word
        widget=Select2Widget(attrs={'data-placeholder':(u"departement")}) #work ok
        #widget=ModelSelect2Widget(  #conflict with Bootstrap
        #    attrs={'data-placeholder': (u"departement") , 'data-minimum-input-length': 0},
        #    model= Department,
         #   search_fields= ['Dep_name__icontains'],
        #    dependent_fields={'employee_list':'employee_list'},
        #   )
           ,required = False
        )
    employee_list = forms.ModelChoiceField(
        queryset=SingleMultiUser_fun().objects.none(),   #dependent dropdown list to be loaded by AJAX 
        #empty_label= u"emp_list",
        widget=Select2Widget(attrs={'data-placeholder':(u"emp_list")}) 
        #widget=ModelSelect2Widget(  #conflict with Bootstrap
        #    attrs={'data-placeholder': (u"emp_list"), 'data-minimum-input-length': 0},
        #    model=Employee,
        #    search_fields=['Full_name__icontains'],
        #    depend_fields={'departement':'departement'},
        #    max_results=500,
        #    )
            ,required = False
        )
        # if 'Department' in self.data:
        #     try:
        #         Department_id = int(self.data.get('Department'))
        #         self.fields['Department'].queryset = SingleMuliUser_fun().objects.filter(Department_id= Department_id).order_by('pk')
        #     except (ValueError, TypeError):
        #         pass  # invalid input from the client; ignore and fallback to empty City queryset
        # elif self.instance.pk:
        #     self.fields['Department'].queryset = self.instance.Department.SingleMuliUser_fun()_set.order_by('pk')


    employee_list_by_name = forms.ModelChoiceField(
        queryset=SingleMultiUser_fun().objects.all().order_by('pk'),   #dependent dropdown list to be loaded by AJAX 
        #empty_label= u"emp_list",
        widget=Select2Widget(attrs={'data-placeholder':(u"Employee Name")}) 
        ,required = False
        )
    employee_list_by_Nid = NidModelChoiceField(
        queryset = SingleMultiUser_fun().objects.all().order_by('pk'),      #values_list('National_id', flat=True).order_by('id'),         # all().order_by('id'),
        widget=Select2Widget(attrs={'data-placeholder':(u"National_id_list")}), 
        required = False, 
        to_field_name= 'id'  #not need as it is the default
        )

    employee_list_by_address = AddModelChoiceField(
        queryset = SingleMultiUser_fun().objects.all().order_by('pk'),     
        widget=Select2Widget(attrs={'data-placeholder':(u"address_list")}), 
        required = False, 
        )
    
    TSdate = forms.DateField(
        input_formats = ['%d/%m/%Y'],
        widget = SelectDateWidget(attrs={'class': 'form-control '}, empty_label=("Choose Year", "Choose Month", "Choose Day"), ),
        required = True,
        )

        