from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render, redirect
from django.db.models import OuterRef, Subquery
from .models import SearchQuery
from employees.models import Employee , Employee_document, Department, Profile#, combine_employee, combine_profile
from django.contrib.auth.models import User
from django.db.models import Q
from .forms import SearchFullName
from Salary.fun_SingleMultiUser import SingleMultiUser_fun


@staff_member_required
@login_required
def search_view(request):
    query= request.GET.get('q',None) #if get from navbar search
    Emp_detail = request.GET.get('employee_list', None) #if get from SearchFullName/employee_list form
    Emp_detail1 = request.GET.get('employee_list_by_Nid', None) ##if get from SearchFullName/employee_list_by_Nid form
    Emp_detail2 = request.GET.get('employee_list_by_address', None)
    TVdate_month = request.GET.get('TSdate_month', None)
    TVdate_Year = request.GET.get('TSdate_year', None)
    emp_ID = request.GET.get('employee_list_by_name', None)
    user= None
    if request.user.is_authenticated:
        user = request.user
        context = {"query":query, "user":user}
        template = 'searches/view.html'
        ############updated#############
        if query:      #==if query is not None:
            #SearchQuery.objects.create(user=user, query=query)  #to save the query and the user name in database
         
            if SingleMultiUser_fun() == Employee: 
            
                employ = Employee.objects.filter(
                    Q(Full_name__icontains = query)|
                    Q(National_id__icontains = query)|
                    Q(Address__icontains = query)|
                    Q(Department__Dep_name__icontains = query)|
                    Q(Emp_date__icontains = query)|
                    Q(First_name__icontains = query)|
                    Q(Last_name__icontains = query)).order_by('id')
            
            elif SingleMultiUser_fun() == Profile:
                employ = Profile.objects.filter(
                    Q(Full_name__username__icontains = query)|
                    Q(National_id__icontains = query)|
                    Q(Address__icontains = query)|
                    Q(Department__Dep_name__icontains = query)|
                    Q(Emp_date__icontains = query)|
                    Q(Full_name__First_name__icontains = query)|
                    Q(Full_name__Last_name__icontains = query)).order_by('id')
                
            context['employ'] = employ
            # context['employ1'] = employ1
            template = 'searches/view.html'
        
         ############updated #############      
        elif Emp_detail:

            if SingleMultiUser_fun().objects.filter(pk = Emp_detail).exists():            
                path = f'/emp/{SingleMuliUser_fun().objects.get(pk = Emp_detail).id}/detail'  
            # elif Profile.objects.filter(Full_name__username = Emp_detail).exists():
            #     path = f'/emp/{Profile.objects.get(Full_name__username = Emp_detail).id}/extend/detail'
            return redirect (path)

        elif Emp_detail1:
                
            path = f'/emp/{Emp_detail1}/detail'  
            return redirect (path)

        elif Emp_detail2:
                    
            path = f'/emp/{Emp_detail2}/detail'  
            return redirect (path)
       
        elif 'Timesheet' in request.GET:  #this is button name in the form multi button
            # print('***************')
            # print("Timesheet",TVdate_month , TVdate_Year, emp_ID)
            path = f'/timesheet/{TVdate_month}/{TVdate_Year}/{emp_ID}'
            return redirect(path)

        elif 'Vacation' in request.GET:  #this is button name in the form multi button
            # print('***************')
            # print("Vacation" , TVdate_Year, emp_ID)
            path = f'/vacation/{TVdate_Year}/{emp_ID}'
            return redirect(path)

        else:
                  
            form = SearchFullName(request.GET or None) #form name from .form.py
            template='searches/view.html'
            context = {"user":user, "form": form}
         

    else:  #not need for that if you make @login_request test before the view
        context = {"NAUTH":'You are not is_authenticated' }
        

    return render(request, template, context)

##########updated #############
@login_required
def load_emp(request):  #for dependent drop down list
    
    departement_id = request.GET.get('depaID')
    employee = SingleMultiUser_fun().objects.filter(Department_id = departement_id ).order_by('pk')
    
    
    #work but what should we do with the deplication in id?? asuming the employee name is unique is not valide for me 
    #employee = Employee.objects.filter(Department_id = departement_id ).values_list('Full_name', flat= True).union(User.objects.filter(profile__Department_id = departement_id).values_list('username', flat= True).exclude(username__icontains ='admin')).exclude(Full_name__icontains ='admin')#Employee.objects.filter(Department_id = departement_id ).order_by('id')
    # print(employee)  # the output in tuple format
    # print(employee[0][0], employee[0][1] )
    # for x in employee:
    #     print(x[0], x[1])
    
    return render(request, 'searches/dep_dropdown_list.html', {'employee':employee})
