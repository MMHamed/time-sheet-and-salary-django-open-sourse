from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.utils   import timezone
from datetime import datetime, date , timedelta
from django.http import HttpResponse, JsonResponse
from django.db.models import Max ,Count
from django.db.models import Q
from django_tables2 import RequestConfig
from .models import vacation
from .forms import vacationFORM
from employees.models import Employee, Profile
from .tables import VacListTBL, ApVacTBL, vacation_Search
from Salary.fun_SingleMultiUser import SingleMultiUser_fun


@login_required
def vacation_view(request):
    ####local function to calculate remaning day of the yearly vacation 
    def RemaningDay(emp_id):  
        qs = SingleMultiUser_fun().objects.get(pk = emp_id )
        emp_date = qs.Emp_date  # employement date
        emp_date_month = int(emp_date.month or 0)
        # to check if the employement data start month is in this year or in last year
        #we asume that vacation year start with the depoyment and end after 12 months
        if  emp_date_month < int(timezone.now().month or 0):
            VacStartDate = date(year= int(timezone.now().year or 0),month= emp_date_month ,day= 1)
            VacEndDate = date(year= (int(timezone.now().year or 0) + 1),month= emp_date_month ,day= 1)
        else:
            VacStartDate = date(year= (int(timezone.now().year or 0) - 1),month= emp_date_month,day= 1)
            VacEndDate = date(year= int(timezone.now().year or 0),month= emp_date_month,day= 1)
        
        if SingleMultiUser_fun() == Employee:
            qs1 = vacation.objects.filter(Emp_name = emp_id, vacation_date__range = (VacStartDate, VacEndDate) ).exclude(submit_date = None).aggregate(AVnum = Count('vacation_date'))
        elif SingleMultiUser_fun() == Profile:
            qs1 = vacation.objects.filter(Pro_name = emp_id, vacation_date__year = timezone.now().year ).exclude(submit_date = None).aggregate(AVnum = Count('vacation_date'))
        
        RemDay = qs.Vacation_per_year - int(qs1['AVnum'] or 0)
        return RemDay 

        ###### end of function ###########    
    if request.POST:
        if SingleMultiUser_fun() == Profile:
            user = request.user   #User.objects.get(pk = Emp_ID)
            if user.groups.filter(name="Self Timesheet and Vacation Request").exists():
                emp_id = Profile.objects.get(Full_name = user.id ).id
                RemDay = RemaningDay(emp_id)  #  function above to calculate the remaining day of vacation this year
                start_date = request.POST.get('start_date', None)
                start_date = datetime.strptime(start_date, '%Y-%m-%d')
                end_date = request.POST.get('End_date', None)
                end_date = datetime.strptime(end_date, '%Y-%m-%d') 
                title = 'Vacation form'
                form = vacationFORM()
                context = {'form':form, 'title':title, 'RemDay':RemDay}
                template='vacations/vacation.html'
                #print(start_date,end_date, emp_id )
        else:
            form = vacationFORM(request.POST or None )
            #print(form.is_valid() , form.errors, form['employee_list'], request.POST.get('employee_list'))
            if form.is_valid():
                # print('*****arrived*********')
                # print (form.cleaned_data['departement'].id, form.cleaned_data['employee_list'].id ) 
                # print (form.cleaned_data['start_date'] , form.cleaned_data['End_date'])
                emp_id = form.cleaned_data['employee_list'].id
                RemDay = RemaningDay(emp_id)  #  function above to calculate the remaining day of vacation this year
                title = 'Vacation form'
                context = {'form':form, 'title':title, 'RemDay':RemDay}
                template='vacations/vacation.html'
                        
                start_date = form.cleaned_data['start_date']
                end_date = form.cleaned_data['End_date']
            
        delta_days = end_date - start_date + timedelta(days=1)
        
        try:
            qs = vacation.objects.aggregate(requestNumb = Max('request_num'))
        except :
            requestNum = int(qs['requestNumb'] or 0)
        else:
            if qs['requestNumb'] == None:
                requestNum = int(qs['requestNumb'] or 0)
            else:
                requestNum = 1 + int(qs['requestNumb'] or 0)
        if SingleMultiUser_fun() == Employee:
            emp =  emp_id
            prof = None 
        elif SingleMultiUser_fun() == Profile:
            emp = None 
            prof = emp_id 
        print(emp_id, requestNum, start_date, end_date)          
        for Vday in range(delta_days.days): #delta_days is not integer but delta_days.days is an integer
            Vdate =  start_date + timedelta(days=Vday)
            obj , created = vacation.objects.get_or_create(
                    Emp_name_id = emp,
                    Pro_name_id = prof,
                    vacation_date = Vdate,
                    defaults={'request_num': requestNum,'submit_date': timezone.now() , 'approve_date':None, 'user':request.user }
            )
            if created: 
                message_return = f'Created/req#{requestNum}'
                context['message']= message_return
                # print(message_return)
            else:
                message_return = 'Requested before'
                context['message']= message_return
                # print(message_return)

        emp_id = request.POST.get('emp_id', None)
        
        if emp_id:
            RemDay = RemaningDay(emp_id)  #  function above to calculate the remaining day of vacation this year 
            return HttpResponse(RemDay)
        
    else:
        if SingleMultiUser_fun() == Profile:
            user = request.user   #User.objects.get(pk = Emp_ID)
            if user.groups.filter(name="Self Timesheet and Vacation Request").exists():
                emp_id = Profile.objects.get(Full_name = user.id ).id
                form = vacationFORM()
                RemDay = RemaningDay(emp_id)
                title = 'Vacation form'
                template='vacations/vacation.html'
                context = {'form':form, 'title':title, 'RemDay':RemDay}
        
        else:
            form = vacationFORM()
            RemDay = 0
            title = 'Vacation form'
            template='vacations/vacation.html'
            context = {'form':form, 'title':title, 'RemDay':RemDay}
    
    return render(request, template, context)


@login_required
def vacation_apporveList_view(request):
    user = request.user   #User.objects.get(pk = Emp_ID)
    if user.groups.filter(name="Approve Timesheet and Vacation for Self Department").exists():
    
        if SingleMultiUser_fun() == Employee:
            dep = Employee.objects.get(id= user.id).Department
            if vacation.objects.filter(approve_date = None).exclude(Q(submit_date = None) |  Q(Emp_name = None)).exists():
                table = VacListTBL(vacation.objects.filter(approve_date = None).exclude(Q(submit_date = None) |  Q(Emp_name = None)).values('Emp_name', 'request_num' ).annotate(dcount=Count('Emp_name')), order_by= "request_num")
                RequestConfig(request, paginate={'per_page': 10}).configure(table)
                context= {'table':table, 'title':'Not approve vacation list' }
            else:
                context= {'title':'There is no vacation to approve' }
        elif SingleMultiUser_fun() == Profile:
            dep = Profile.objects.get(Full_name_id = user.id).Department
            
            if vacation.objects.filter(approve_date = None, Pro_name__Department = dep).exclude(Q(Pro_name = None)| Q(submit_date = None)).exists():
                table = VacListTBL(vacation.objects.filter(approve_date = None, Pro_name__Department = dep).exclude(Q(Pro_name = None)| Q(submit_date = None)).values('Pro_name', 'request_num' ).annotate(dcount=Count('Pro_name')), order_by= "request_num")
                RequestConfig(request, paginate={'per_page': 10}).configure(table)
                context= {'table':table, 'title':'Not approve vacation list' }
            else:
                context= {'title':'There is no vacation to approve' }
    elif user.is_superuser:
        if SingleMultiUser_fun() == Employee:
            if vacation.objects.filter(approve_date = None).exclude(Q(submit_date = None) |  Q(Emp_name = None)).exists():
                table = VacListTBL(vacation.objects.filter(approve_date = None).exclude(Q(submit_date = None) |  Q(Emp_name = None)).values('Emp_name', 'request_num' ).annotate(dcount=Count('Emp_name')), order_by= "request_num")
                RequestConfig(request, paginate={'per_page': 10}).configure(table)
                context= {'table':table, 'title':'Not approve vacation list' }
            else:
                context= {'title':'There is no vacation to approve' }
        elif SingleMultiUser_fun() == Profile:
            if vacation.objects.filter(approve_date = None).exclude(Q(Pro_name = None)| Q(submit_date = None)).exists():
                table = VacListTBL(vacation.objects.filter(approve_date = None).exclude(Q(Pro_name = None)| Q(submit_date = None)).values('Pro_name', 'request_num' ).annotate(dcount=Count('Pro_name')), order_by= "request_num")
                RequestConfig(request, paginate={'per_page': 10}).configure(table)
                context= {'table':table, 'title':'Not approve vacation list' }
            else:
                context= {'title':'There is no vacation to approve' }     
            
    # context= {'table':table, 'title':'Non approve vacation list' }
    template = 'vacations/NAVacList.html'
    return render(request, template, context)

   
@login_required
def vacation_apporve_view(request, emp_id, req_num):
    if SingleMultiUser_fun() == Employee:
        table = ApVacTBL(vacation.objects.filter(Emp_name = emp_id, request_num = req_num), order_by= "request_num")
    elif SingleMultiUser_fun() == Profile:
        table = ApVacTBL(vacation.objects.filter(Pro_name = emp_id, request_num = req_num), order_by= "request_num")
    qs = SingleMultiUser_fun().objects.get(id = emp_id)
    RequestConfig(request, paginate={'per_page': 10}).configure(table)
    title = f'Need to approve vacation for employee: {qs} for request# {req_num} ' 
    context= {'table':table, 'title':title, 'NAVac':True }
    template = 'vacations/NAVacList.html'

    if request.POST:
        stat = request.POST.get('status' ,  None)
        if SingleMultiUser_fun()== Employee: 
            qs = vacation.objects.filter( Emp_name_id = emp_id , request_num = req_num )
        elif SingleMultiUser_fun() == Profile:
            qs = vacation.objects.filter( Pro_name_id = emp_id , request_num = req_num )
        if stat == 'approve':
            qs.update(approve_date = timezone.now(), user = request.user )
            return HttpResponse("approved successfully")
        elif stat == 'reject': # submit_date = None
            qs.update(submit_date = None, user = request.user ) # We don't delete the record in order to save it in database as rejected (submit = None). Plus we want to keep request number sequence  
            return HttpResponse("Rejected successfully")

    return render(request, template, context)

@login_required
def vacationSearch_view(request,TVdate_Year, emp_ID):
    if SingleMultiUser_fun() == Employee:
        table = vacation_Search(vacation.objects.filter(Emp_name = emp_ID, vacation_date__year = TVdate_Year  )) 
    elif SingleMultiUser_fun() == Profile: 
        table = vacation_Search(vacation.objects.filter(Pro_name = emp_ID, vacation_date__year = TVdate_Year  )) 
    template = 'vacations/NAVacList.html'
    context = {'table':table, 'title':f'Vacation status list for employee_id: {emp_ID} for year: {TVdate_Year}'}
    return render(request, template, context)
    