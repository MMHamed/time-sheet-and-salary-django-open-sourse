import django_tables2 as tables
from django import forms
import itertools
from django_tables2.utils import A
from django.utils.safestring import mark_safe
from timesheets.models import TimeSheet
from employees.models import Employee, Department, Profile
from .models import vacation
from Salary.fun_SingleMultiUser import SingleMultiUser_fun

class VacListTBL(tables.Table):
    row_number = tables.Column(empty_values=()) #to add a row that increament each row->it work
    Department = tables.Column(empty_values=(), orderable=False)
    employee_name = tables.Column(empty_values=(), orderable=False)
    Employee_ID = tables.Column(empty_values=(), orderable=False)
    #Emp_name = tables.LinkColumn('approveVacation', args=[A('Emp_name'), A('request_num')], verbose_name='Employee_ID') #to make a row on click go to timeSheet; not work as the below overwrite
    #Emp_name = tables.Column(verbose_name='Employee_ID') # not working alone, we have to combine it with the above
    class Meta:
        model = vacation
        fields= [ 'request_num',  'dcount']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'Employee_ID', 'employee_name', 'Department', 'request_num', 'dcount']

     
   
    
    def __init__(self, *args, **kwargs):
        super(VacListTBL, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return 'Row %d' % next(self.counter)

    def render_employee_name(self, value ,record): #this work. with its declaration above we insert a new column that contain the name of the employee!!! 
        if SingleMultiUser_fun() == Employee:
            qs= SingleMultiUser_fun().objects.get(id = record['Emp_name'])
            return mark_safe('''<a href=/%s/%s/vacationApprove>%s</a>''' % (record['Emp_name'],record['request_num'],qs))
        elif SingleMultiUser_fun() == Profile:
            qs= SingleMultiUser_fun().objects.get(id = record['Pro_name'])
            return mark_safe('''<a href=/%s/%s/vacationApprove>%s</a>''' % (record['Pro_name'],record['request_num'],qs))

    def render_Employee_ID(self, value ,record): #this work. with its declaration above we insert a new column that contain the name of the employee!!! 
        if SingleMultiUser_fun() == Employee:
            qs= SingleMultiUser_fun().objects.get(id = record['Emp_name'])
            return mark_safe('''<a href=/%s/%s/vacationApprove>%s</a>''' % (record['Emp_name'],record['request_num'],qs.id))
        elif SingleMultiUser_fun() == Profile:
            qs= SingleMultiUser_fun().objects.get(id = record['Pro_name'])
            return mark_safe('''<a href=/%s/%s/vacationApprove>%s</a>''' % (record['Pro_name'],record['request_num'],qs.id))    

    def render_Department(self, value , record): #this work. with its declaration above we insert a new column that contain the name of the employee!!! 
        if SingleMultiUser_fun() == Employee:
            qs= SingleMultiUser_fun().objects.get(id = record['Emp_name'])
        elif SingleMultiUser_fun() == Profile:
            qs= SingleMultiUser_fun().objects.get(id = record['Pro_name'])
        return '%s ' % qs.Department



class ApVacTBL(tables.Table):
    row_number = tables.Column(empty_values=())
    Emp_name = tables.Column(empty_values=(), verbose_name= 'Employee Name')
    class Meta:
        model = vacation
        fields= [ 'vacation_date','request_num', 'submit_date']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'Emp_name', 'vacation_date','request_num', 'submit_date']

    def __init__(self, *args, **kwargs):
        super(ApVacTBL, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return 'Row %d' % next(self.counter)

    def render_Emp_name(self, value, record):
        if SingleMultiUser_fun() == Employee:
            return '%s' %record.Emp_name
        elif SingleMultiUser_fun() == Profile:
            return '%s' %record.Pro_name



class vacation_Search(tables.Table):
    row_number = tables.Column(empty_values=())
    request_status = tables.Column(empty_values=())
    Emp_name = tables.Column(empty_values=(), verbose_name='Employee name')
    class Meta:
        model = vacation
        fields= [ 'vacation_date','request_num']
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ['row_number', 'Emp_name', 'vacation_date','request_num', 'request_status']

    def __init__(self, *args, **kwargs):
        super(vacation_Search, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return 'Row %d' % next(self.counter)

    def render_request_status(self, value , record):  #work too !!!!!
        if SingleMuliUser_fun() == Employee:
            qs= vacation.objects.get(Emp_name = record.Emp_name, vacation_date= record.vacation_date )
        elif SingleMuliUser_fun() == Profile:
            qs= vacation.objects.get(Pro_name = record.Emp_name, vacation_date= record.vacation_date )
        if qs.submit_date:
            if qs.approve_date:
                stat= 'Approved'
            else:
                stat = 'Submitted'
        else:
            stat = 'Rejected'
        return '%s ' % stat         # % record.Emp_name + str(record.vacation_date) 

    def render_Emp_name(self, value, record):
        if SingleMuliUser_fun() == Employee:
            return '%s' %record.Emp_name
        elif SingleMuliUser_fun() == Profile:
            return '%s' %record.Pro_name