from django.db import models
from django.conf import settings
from employees.models import Employee, Profile
from Salary.fun_SingleMultiUser import SingleMultiUser_fun

class vacation(models.Model):
    Emp_name = models.ForeignKey(Employee, null= True, on_delete=models.CASCADE)
    Pro_name= models.ForeignKey(Profile,null = True, on_delete=models.CASCADE)
    vacation_date = models.DateField(blank=True, null=True, auto_now_add=False)
    request_num = models.DecimalField(max_digits=12, decimal_places=0,null=True)
    submit_date = models.DateField(blank=True, null=True, auto_now_add=False)
    approve_date = models.DateField(blank=True, null=True, auto_now_add=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    updated = models.DateTimeField(auto_now=True)
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['Prof_name','Emp_name','vacation_date'],name='unique_vacation')
            #each employee have only one vaction in the same day 
        ]
    if SingleMultiUser_fun() == Employee:
        def __str__(self):
            return self.Emp_name.Full_name
    
    elif SingleMultiUser_fun() == Profile:
        def __str__(self):
            return self.Pro_name.Full_name.username
