from django import forms
from django_select2.forms import Select2Widget
from django.forms import ModelChoiceField, widgets, SelectDateWidget, DateInput
from employees.models import Employee, Department
from django.contrib.admin import widgets

class vacationFORM(forms.Form):
    departement = forms.ModelChoiceField(
        queryset=Department.objects.all(),
        widget=Select2Widget(attrs={'data-placeholder':(u"Departement")})
        ,required = False
        )
    employee_list = forms.ModelChoiceField(
        queryset=Employee.objects.none(),   
        widget=Select2Widget(attrs={'data-placeholder':(u"Employee_Name"), 'class':'vac_emp_name'}) 
        ,required = False
        )
    
    start_date= forms.DateField(
        input_formats = ['%Y-%m-%d'],
        widget = DateInput(attrs={'class': 'form-control ', 'type':'date' }, )
        ,required = True
        )
    End_date = forms.DateField(
        input_formats = ['%Y-%m-%d'],
        widget = DateInput(attrs={'class': 'form-control ', 'type':'date'}, )
        ,required = True
        )

    def __init__(self, *args, **kwargs): #we do this because the form.is_valid() is always False and the emp is always empty. dependent dropdown list
        super().__init__(*args, **kwargs)
    

        if 'departement' in self.data:
            try:
                Department_id= int(self.data.get('departement'))
                self.fields['employee_list'].queryset = Employee.objects.filter(Department_id=Department_id).order_by('id')
            except(ValueError,TypeError):
                pass
        # elif self.instance.pk:  #there is no update in that form
        #     self.fields['employee_list'].queryset = self.instance.Departement.Employee_set.order_by('id')


