# Django Time sheet and Salary


## Running Locally

```bash
git clone https://MMHamed@bitbucket.org/MMHamed/time-sheet-and-salary-django-project.git/django_local_library.git
```

```bash
cd django_local_library
```

```bash
pip install -r requirements.txt
```

```bash
python manage.py migrate
```

```bash
python manage.py collectstatic  
```

```bash
python manage.py createsuperuser  
```


```bash
python manage.py runserver
```
